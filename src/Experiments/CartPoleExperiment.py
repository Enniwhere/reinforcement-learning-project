__author__ = 'simon'

import sys
import math
import rlglue.RLGlue as RLGlue


#
#	This function will freeze the agent's policy and test it after every 25 episodes.
#
number = 1
def offlineDemo():
    this_score=evaluateAgent()
    printScore(0,this_score)
    data={}
    for i in range(0,100):
        for j in range(0,10):
            RLGlue.RL_episode(1000)
            #data[100*i+j] = RLGlue.RL_num_steps()
        data[i]=evaluateAgent()
        printScore((i+1)*10,data[i])

    saveResultToCSV(data,"CartPoleExperimentResults sutton" + str(number) + ".csv")

def printScore(afterEpisodes, score):
    print "%d\t\t%.2f" % (afterEpisodes, score)

#
# Tell the agent to stop learning, then execute n episodes with his current
# policy.  Estimate the mean and variance of the return over these episodes.
#
def evaluateAgent():
    sum=0
    n=10

    RLGlue.RL_agent_message("freeze_learning")
    RLGlue.RL_agent_message("freeze_exploring")
    for i in range(0,n):
        # We use a cutoff here in case the
        #policy is bad and will never end an episode
        RLGlue.RL_episode(1000)
        this_return=RLGlue.RL_num_steps()
        sum+=this_return

    mean=sum/n
    #standard_dev=math.sqrt(variance)

    RLGlue.RL_agent_message("unfreeze_learning")
    RLGlue.RL_agent_message("unfreeze_exploring")
    return mean


def saveResultToCSV(data, fileName):
    theFile = open(fileName, "w")
    theFile.write("#Results from CartPoleExperiment.py.  First line is means, second line is standard deviations.\n")

    for i in range(0,len(data)):
        theFile.write(str(i) + ", %.2f\n" % data[i])

    theFile.close()


#
# Just do a single evaluateAgent and print it
#
def	single_evaluation():
    this_score=evaluateAgent()
    printScore(0,this_score)



print "Starting offline demo\n----------------------------\nWill alternate learning for 25 episodes, then freeze policy and evaluate for 10 episodes.\n"
print "After Episode\tMean Return\tStandard Deviation\n-------------------------------------------------------------------------"
for i in range(1,6):
    number = i
    RLGlue.RL_init()
    #0.32, 1.00, 0.25, 0.30, 1.00
    RLGlue.RL_agent_message("set_parameters " + str(0.64) + " " + str(1.0) + " " + str(0.20) + " " + str(1.0) + " " + str(1.0))
    #RLGlue.RL_agent_message("set_parameters " + str(0.08) + " " + str(1.0) + " " + str(0.3) + " " + str(0.3) + " " + str(0.50))
    offlineDemo()
    RLGlue.RL_agent_message("save_policy " + str(number))
    RLGlue.RL_cleanup()

#print "\nNow we will save the agent's learned value function to a file...."

#RLGlue.RL_agent_message("save_policy results.dat")

#print "\nCalling RL_cleanup and RL_init to clear the agent's memory..."

#RLGlue.RL_cleanup()
#RLGlue.RL_init()

#print "Evaluating the agent's default policy:\n\t\tMean Return\tStandardDeviation\n------------------------------------------------------"
#single_evaluation()

#print "\nLoading up the value function we saved earlier."
#RLGlue.RL_agent_message("load_policy results.dat")

#print "Evaluating the agent after loading the value function:\n\t\tMean Return\tStandardDeviation\n------------------------------------------------------"
#single_evaluation()

#print "Telling the environment to use fixed start state of 2,3."
#RLGlue.RL_env_message("set-start-state 2 3")
#RLGlue.RL_start()
#print "Telling the environment to print the current state to the screen."
#RLGlue.RL_env_message("print-state")
#print "Evaluating the agent a few times from a fixed start state of 2,3:\n\t\tMean Return\tStandardDeviation\n-------------------------------------------"
#single_evaluation()

#print "Evaluating the agent again with the random start state:\n\t\tMean Return\tStandardDeviation\n-----------------------------------------------------"
#RLGlue.RL_env_message("set-random-start-state")
#single_evaluation()


#RLGlue.RL_cleanup()
print "\nProgram Complete."



