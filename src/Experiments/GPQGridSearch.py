
__author__ = 'simon'

import sys
import math
import rlglue.RLGlue as RLGlue
import pickle



def gridSearch(kernel_bandwidth, kernel_placement, exploration_rate, discount_factor, learning_rate):
    this_score=0
    total_score= this_score
    printScore(0,total_score)
    RLGlue.RL_agent_message("set_parameters " + str(kernel_bandwidth) + " " + str(kernel_placement) + " " + str(exploration_rate) + " " + str(discount_factor) + " " + str(learning_rate))
    for i in range(0,5):
        for j in range(0,50):
            RLGlue.RL_episode(1000)
    this_score=evaluateAgent()
    printScore((i+1)*50,this_score)
    total_score+=this_score

    return total_score




def printScore(afterEpisodes, score):
    print "%d\t\t%.2f" % (afterEpisodes, score)

#
# Tell the agent to stop learning, then execute n episodes with his current
# policy.  Estimate the mean and variance of the return over these episodes.
#
def evaluateAgent():

    n = 100
    balancing_steps = 0
    RLGlue.RL_agent_message("freeze_learning")
    RLGlue.RL_agent_message("freeze_exploring")
    for i in range(0,n):
        # We use a cutoff here in case the
        #policy is bad and will never end an episode
        RLGlue.RL_episode(1000)
        this_return=RLGlue.RL_return()
        balancing_steps += RLGlue.RL_num_steps()


    #standard_dev=math.sqrt(variance)

    RLGlue.RL_agent_message("unfreeze_learning")
    RLGlue.RL_agent_message("unfreeze_exploring")

    #print("Agent balanced for " + str(balancing_steps) + " steps.")
    return balancing_steps


def saveResultToCSV(statistics, fileName):
    theFile = open(fileName, "w")
    theFile.write("#Results from sample_experiment.py.  First line is means, second line is standard deviations.\n")

    for thisEntry in statistics:
        theFile.write("%.2f, " % thisEntry[0])
    theFile.write("\n")

    for thisEntry in statistics:
        theFile.write("%.2f, " % thisEntry[1])
    theFile.write("\n")

    theFile.close()


#
# Just do a single evaluateAgent and print it
#
def	single_evaluation():
    this_score=evaluateAgent()
    printScore(0,this_score)


#kernel_bandwidths = [0.25, 0.5, 1.0]
kernel_placements = [1.0]
exploration_rates = [0.1, 0.2, 0.3]
#discount_factors = [0.3, 0.5, 0.7]
#learning_rates = [0.7, 0.8, 0.9]
kernel_bandwidths = [0.48, 0.64, 0.86, 1.0]
#kernel_placements = [0.5, 0.8, 1.0, 1.2, 2.0]
#exploration_rates = [0.5]
discount_factors = [1.0]
#learning_rates = [0.5, 0.7, 0.9]
learning_rates = [1.0]

print "Starting grid search\n----------------------------\nWill alternate learning for 250 episodes, then freeze policy and evaluate for 100 episodes for different parameters.\n"
print "After Episode\tBalancing steps\tParameters\n-------------------------------------------------------------------------"
RLGlue.RL_init()
for bandwidth in kernel_bandwidths:
    for placement in kernel_placements:
        for exploration_rate in exploration_rates:
            for discount_factor in discount_factors:
                for learning_rate in learning_rates:
                    RLGlue.RL_cleanup()
                    RLGlue.RL_init()
                    total_score = gridSearch(bandwidth, placement, exploration_rate, discount_factor, learning_rate)
                    print "Score: " + str(total_score) + " for parameters (%.2f, %.2f, %.2f, %.2f, %.2f)" % (bandwidth, placement, exploration_rate, discount_factor, learning_rate)

                    theFile = open("GPQ GS Params " + str(bandwidth) + " " + str(placement) + " " + str(exploration_rate) + " " + str(discount_factor) + " " + str(learning_rate), "w")
                    pickle.dump(str(total_score), theFile)

                    theFile.close()
RLGlue.RL_agent_message("save policy")

#print "\nNow we will save the agent's learned value function to a file...."

#RLGlue.RL_agent_message("save_policy results.dat")

#print "\nCalling RL_cleanup and RL_init to clear the agent's memory..."

#RLGlue.RL_cleanup()
#RLGlue.RL_init()

#print "Evaluating the agent's default policy:\n\t\tMean Return\tStandardDeviation\n------------------------------------------------------"
#single_evaluation()

#print "\nLoading up the value function we saved earlier."
#RLGlue.RL_agent_message("load_policy results.dat")

#print "Evaluating the agent after loading the value function:\n\t\tMean Return\tStandardDeviation\n------------------------------------------------------"
#single_evaluation()

#print "Telling the environment to use fixed start state of 2,3."
#RLGlue.RL_env_message("set-start-state 2 3")
#RLGlue.RL_start()
#print "Telling the environment to print the current state to the screen."
#RLGlue.RL_env_message("print-state")
#print "Evaluating the agent a few times from a fixed start state of 2,3:\n\t\tMean Return\tStandardDeviation\n-------------------------------------------"
#single_evaluation()

#print "Evaluating the agent again with the random start state:\n\t\tMean Return\tStandardDeviation\n-----------------------------------------------------"
#RLGlue.RL_env_message("set-random-start-state")
#single_evaluation()


#RLGlue.RL_cleanup()
print "\nProgram Complete."



