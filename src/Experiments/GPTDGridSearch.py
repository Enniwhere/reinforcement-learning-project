
__author__ = 'simon'

import sys
import math
import thread
import rlglue.RLGlue as RLGlue
import pickle



def gridSearch(kernel_bandwidth, discount_factor, intrinsic_variance):
    this_score=evaluateAgent()
    total_score= this_score
    printScore(0,total_score)
    RLGlue.RL_agent_message("set_parameters " + str(kernel_bandwidth) + " " + str(discount_factor) + " " + str(intrinsic_variance))

    for i in range(0,5):
        for j in range(0,50):
            RLGlue.RL_episode(1000)
    this_score=evaluateAgent()
    printScore((i+1)*50,this_score)
    total_score+=this_score
    return total_score

def printScore(afterEpisodes, score):
    print "%d\t\t%.2f" % (afterEpisodes, score)

#
# Tell the agent to stop learning, then execute n episodes with his current
# policy.  Estimate the mean and variance of the return over these episodes.
#
def evaluateAgent():

    n = 100
    balancing_steps = 0
    RLGlue.RL_agent_message("freeze_learning")
    RLGlue.RL_agent_message("freeze_exploring")
    for i in range(0,n):
        # We use a cutoff here in case the
        #policy is bad and will never end an episode
        RLGlue.RL_episode(1000)
        this_return=RLGlue.RL_return()
        balancing_steps += RLGlue.RL_num_steps()


    #standard_dev=math.sqrt(variance)

    RLGlue.RL_agent_message("unfreeze_learning")
    RLGlue.RL_agent_message("unfreeze_exploring")

    #print("Agent balanced for " + str(balancing_steps) + " steps.")
    return balancing_steps


def saveResultToCSV(statistics, fileName):
    theFile = open(fileName, "w")
    theFile.write("#Results from sample_experiment.py.  First line is means, second line is standard deviations.\n")

    for thisEntry in statistics:
        theFile.write("%.2f, " % thisEntry[0])
    theFile.write("\n")

    for thisEntry in statistics:
        theFile.write("%.2f, " % thisEntry[1])
    theFile.write("\n")

    theFile.close()


#
# Just do a single evaluateAgent and print it
#
def	single_evaluation():
    this_score=evaluateAgent()
    printScore(0,this_score)


kernel_bandwidths = [0.1, 0.2, 0.4, 0.6, 0.8, 1.0]
discount_factors = [0.3, 0.6, 0.9]
intrinsic_variances = [0.02, 0.08, 0.32]
print "Starting grid search\n----------------------------\nWill alternate learning for 100 episodes, then freeze policy and evaluate for 10 episodes for different parameters.\n"
print "After Episode\tBalancing steps\tParameters\n-------------------------------------------------------------------------"
RLGlue.RL_init()
for bandwidth in kernel_bandwidths:
    for discount_factor in discount_factors:
        for variance in intrinsic_variances:

            RLGlue.RL_cleanup()
            RLGlue.RL_init()
            total_score = gridSearch(bandwidth, discount_factor, variance)
            print "Score: " + str(total_score) + " for parameters (%.2f, %.2f, %.2f)" % (bandwidth, discount_factor, variance)
            theFile = open("GPTD GS Params " + str(bandwidth) + " " + str(discount_factor) + " " + str(variance), "w")
            pickle.dump(str(total_score), theFile)

            theFile.close()

#print "\nNow we will save the agent's learned value function to a file...."

#RLGlue.RL_agent_message("save_policy results.dat")

#print "\nCalling RL_cleanup and RL_init to clear the agent's memory..."

#RLGlue.RL_cleanup()
#RLGlue.RL_init()

#print "Evaluating the agent's default policy:\n\t\tMean Return\tStandardDeviation\n------------------------------------------------------"
#single_evaluation()

#print "\nLoading up the value function we saved earlier."
#RLGlue.RL_agent_message("load_policy results.dat")

#print "Evaluating the agent after loading the value function:\n\t\tMean Return\tStandardDeviation\n------------------------------------------------------"
#single_evaluation()

#print "Telling the environment to use fixed start state of 2,3."
#RLGlue.RL_env_message("set-start-state 2 3")
#RLGlue.RL_start()
#print "Telling the environment to print the current state to the screen."
#RLGlue.RL_env_message("print-state")
#print "Evaluating the agent a few times from a fixed start state of 2,3:\n\t\tMean Return\tStandardDeviation\n-------------------------------------------"
#single_evaluation()

#print "Evaluating the agent again with the random start state:\n\t\tMean Return\tStandardDeviation\n-----------------------------------------------------"
#RLGlue.RL_env_message("set-random-start-state")
#single_evaluation()


#RLGlue.RL_cleanup()
print "\nProgram Complete."



