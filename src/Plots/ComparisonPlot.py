__author__ = 'simon'

import numpy as np
import matplotlib.pyplot as pyplot
import csv

data = {}
with open("CartPoleExperimentResults bigger budget bandwidth 0.7.csv", "rb") as csvfile:
    reader = csv.reader(csvfile)
    first_row = True
    for row in reader:
        if first_row:
            first_row = False
        else:
            data[int(row[0])] = float(row[1])
    pyplot.plot(range(0,1000,10),data.values(), color="blue", linewidth=2.5, linestyle='-', label='GPQ $\epsilon$-greedy')

with open("CartPoleExperimentResults sutton1.csv", "rb") as csvfile:
    reader = csv.reader(csvfile)
    first_row = True
    for row in reader:
        if first_row:
            first_row = False
        else:
            data[int(row[0])] = float(row[1])
    pyplot.plot(range(0,1000,10),data.values(), color="red", linewidth=2.5, linestyle='--', label="Sutton's actor-critic")

pyplot.ylim([0,1050])
pyplot.legend(loc='lower right', frameon=False)
pyplot.xlabel("Number of episodes trained")
pyplot.ylabel("Number of balancing steps")
#print str(row)

pyplot.suptitle("Comparison of different learning strategies", fontsize=18)


#for index in data:
#    print str(index) + ", " + str(data[index])


pyplot.show()