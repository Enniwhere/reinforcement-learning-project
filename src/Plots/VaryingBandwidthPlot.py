__author__ = 'simon'

import numpy as np
import matplotlib.pyplot as pyplot
import csv


f, axarr = pyplot.subplots(2,3, sharex="col", sharey="row")

for i in range(0,6):
    #print "Opening file " + str(i)
    data = {}
    with open("CartPoleExperimentResults bandwidth " + str(0.1 + i*0.2) + ".csv", "rb") as csvfile:
        reader = csv.reader(csvfile)
        first_row = True
        for row in reader:
            if first_row:
                first_row = False
            else:
                data[int(row[0])] = float(row[1])
    #print str(row)

    axarr[i/3, i%3].plot(range(0,1000,10), data.values())
    axarr[i/3, i%3].set_title("Bandwidth: " + str(0.1 + i*0.2))
    axarr[i/3, i%3].set_xlim([0,1000])
    axarr[i/3, i%3].set_ylim([0,1050])

f.text(0.5, 0.04, 'Number of episodes trained', ha='center', va='center')
f.text(0.06, 0.5, 'Number of balacing steps', ha='center', va='center', rotation='vertical')
f.suptitle("$\epsilon$-greedy exploration with budget 40", fontsize=18)


#for index in data:
#    print str(index) + ", " + str(data[index])


pyplot.show()