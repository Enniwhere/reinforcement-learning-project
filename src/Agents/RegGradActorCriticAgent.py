__author__ = 'simon'

import random
import sys
import copy
import pickle
from rlglue.agent.Agent import Agent
from rlglue.agent import AgentLoader as AgentLoader
from rlglue.types import Action
from rlglue.types import Observation
from rlglue.utils import TaskSpecVRLGLUE3
from random import Random
import numpy as np
import math
from math import pi as PI

NUM_WEIGHTS = 0

class RegGradActorCriticAgent(Agent):

    lastObservation = None
    num_steps = 0

    c = 0.95
    theta = np.zeros(NUM_WEIGHTS)
    v = np.zeros(NUM_WEIGHTS)

    alpha = 0
    beta = 0
    xi = 0


    def agent_init(self,taskSpecString):
        TaskSpec = TaskSpecVRLGLUE3.TaskSpecParser(taskSpecString)
        if TaskSpec.valid:
            assert len(TaskSpec.getDoubleObservations())==4, "expecting 4 continuous observations"

            assert len(TaskSpec.getDoubleActions())==1, "expecting 1 continuous action"

        else:
            print "Task Spec could not be parsed: "+taskSpecString

        self.lastObservation=Observation()



    def agent_start(self,observation):
        theState=observation.doubleArray
        self.lastObservation = observation
        self.num_steps = 0

        returnAction=Action()

        return returnAction


    def agent_step(self,reward, observation):


        self.num_steps += 1
        newState=observation.doubleArray

        returnAction=Action()

        return returnAction

    def agent_end(self,reward):
        lastState=self.lastObservation.doubleArray

        pass


    def agent_cleanup(self):
        pass

    def save_value_function(self, fileName):
        theFile = open(fileName, "w")
        pickle.dump(self.value_function, theFile)
        theFile.close()

    def load_value_function(self, fileName):
        theFile = open(fileName, "r")
        self.value_function=pickle.load(theFile)
        theFile.close()

    def agent_message(self,inMessage):

        #Message Description
        # save_policy FILENAME
        # Action: Save current value function in binary format to
        # file called FILENAME
        #
        if inMessage.startswith("save policy"):
            splitString=inMessage.split(" ")
            self.save_value_function(splitString[1])
            print "Saved."
            return "message understood, saving policy"

        #Message Description
        # load_policy FILENAME
        # Action: Load value function in binary format from
        # file called FILENAME
        #
        #if inMessage.startswith("load policy"):
        #    splitString=inMessage.split(" ")
        #    self.load_value_function(splitString[1])
        #    print "Loaded."
        #    return "message understood, loading policy"

        return "SuttonAgent(Python) does not understand your message."

    def save_value_function(self, fileName):
        theFile = open(fileName, "w")
        pickle.dump(self.w, theFile)
        theFile.close()


if __name__=="__main__":
    AgentLoader.loadAgent(RegGradActorCriticAgent())