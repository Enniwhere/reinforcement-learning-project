__author__ = 'simon'



import random
import sys
import copy
import pickle
from rlglue.agent.Agent import Agent
from rlglue.agent import AgentLoader as AgentLoader
from rlglue.types import Action
from rlglue.types import Observation
from rlglue.utils import TaskSpecVRLGLUE3
from random import Random
import numpy as np
import math
from math import pi as PI





class GPTDAgent(Agent):

    KERNEL_BANDWIDTH = 0.70
    GAMMA = 0.5

    STATE_C = 1.0
    STATE_SIGMA = 0.001
    ACTION_C = 1.0
    ACTION_SIGMA = 2.0
    ACTION_SCALE = 10.0
    sarsa_epsilon = 0.1
    random_sample_size = 100

    nu = 0.2                     # The threshold for adding new points to BasisVectors
    sigma2 = 0.5                      # The intrinsic variance of the system

    BasisVectors = None                     # The dictionary holding the points used for approximating the gaussian process
    K_tilde = None    # An approximation of the inverse of the kernel matrix
    a = None                        # A BasisVectors x 1 vector of optimal approximation coefficients
    alpha_tilde = None                    # A BasisVectors x 1 vector of approximated mean parameters
    C_tilde = None                        # A BasisVectors x BasisVectors matrix of approximated covariance parameters
    c_tilde = None                        # A BasisVectors x 1 approximate-gain-vector used for adjusting alpha_tilde
    d = 0                           # A variable used for scaling c
    s = float('inf')                      # 1/s used for normalizing c
    T = 0                           # variable holding the episode number
    kernel_vector = None
    kernel_matrix = None
    h = None

    t = 0
    totalsteps = 0

    lastObservation = None
    lastAction = None

    action_spans = [(0,0)]
    state_spans = [(0,0),(0,0),(0,0),(0,0)]
    action_range = None
    valid_actions = [-1.0, -0.5, -0.25, 0, 0.25, 0.5, 1.0]

    explorationFrozen = False
    learningFrozen = False


    def stateActionPair(self,obs,action):
        # print "Tried to make a state-action pair from " + str(obs) + " and " +str(action)
        return np.concatenate((np.array(obs), np.array(action)),axis=1)

    def RBFkernel(self,z1,z2):
        # print "Tried to take the kernel value of " + str(z1) + " and " + str(z2)
        val = math.exp(-(np.linalg.norm(np.array(z1)-np.array(z2))**2) / (2 * self.KERNEL_BANDWIDTH))
        return val

    # This function returns the element-wise product of the two kernel vectors above
    def kernelVector(self,obs,act):
        arr = [[self.RBFkernel(self.BasisVectors[x],self.stateActionPair(obs,act))] for x in self.BasisVectors]
        #print "kernel vector is " + str(arr)
        return np.atleast_2d(arr)

    def fastKernelVector(self, obs, act):
        z = np.atleast_2d(self.stateActionPair(obs, act))
        BVs = np.atleast_2d(self.BasisVectors.values())
        #print "Shape of BVs: " + str(BVs.shape) + ", shape of z: " + str(z.shape)
        squared_and_divided = np.square(np.linalg.norm(BVs-z,axis=1))/(2*self.KERNEL_BANDWIDTH**2)
        kernel_vector = np.atleast_2d(np.exp(-squared_and_divided))

        #print "Shape of fast kernel_vector: " + str(kernel_vector.shape)

        return kernel_vector.T

    # This function computes the kernel matrix from the basis vectors
    def kernelMatrix(self):
        n = len(self.BasisVectors)
        return [[self.RBFkernel(l,m) for l in range(0,n-1)] for m in range(0,n-1)]

    def normalize_state(self,state):
         #return np.array([(state[i]-self.state_spans[i][0])/(self.state_spans[i][1] - self.state_spans[i][0]) for i in range(1,len(state))])
         return np.array([(2*state[i])/(self.state_spans[i][1] - self.state_spans[i][0]) for i in range(0,4)])
         #return np.array([(2*state[i])/(self.state_spans[i][1] - self.state_spans[i][0]) for i in range(2,4)])


    # This function calculates the approximation of the value of a given action based on accumulated experience
    def approxActionValue(self, obs, action):
        return np.linalg.norm(np.dot(self.alpha_tilde.T, self.fastKernelVector(obs,action)))

    # This function calculates the approximation of the value of a given action based on accumulated experience
    def approxQ(self, obs, act):
        return np.dot(self.alpha_tilde.T,self.fastKernelVector(obs,act))

    def greedyAction(self,obs):
        Qs = np.array([self.approxQ(obs,[x]) for x in self.valid_actions])
        action_index = random.randint(0, len(self.valid_actions)-1)
        for i in range(0, len(self.valid_actions)):
            if Qs[i] > Qs[action_index]:
                action_index = i
        #print "Nonrandom action: \t\t" + str(self.valid_actions[action_index]) + " for timestep " + str(self.steps)
        #print "Picked action number " + str(action_index) + " from Q-array:"
        #print str(Qs)
        return np.array([self.valid_actions[action_index]])

    # This function chooses a uniformly random action from the action span
    def randomAction(self):
        return [random.random()*(self.action_spans[0][1] - self.action_spans[0][0]) + self.action_spans[0][0]]

    # This function samples a number of random actions and picks the one with the maximum estimated value among them
    def pickFromRandomActionSample(self,obs):

        arr = [self.randomAction() for x in range(1,self.random_sample_size)]
        vals = [self.approxActionValue(obs,arr[x]) for x in range(0,self.random_sample_size-1)]
        #print "Picking action among: " + str(arr)
        #print "With values: " + str(vals)
        return arr[np.argmax(vals)]

    def pickFromRange(self,obs):
        vals = [self.approxActionValue(obs,[self.action_range[x]]) for x in range(0,self.random_sample_size-1)]
        #print "Picking action among: " + str(arr)
        #print "With values: " + str(vals)
        return [self.action_range[np.argmax(vals)]]


    # Classical epsilon-greedy action selection
    def epsilonGreedy(self,obs):
        if random.random() < self.sarsa_epsilon:
            action = self.randomAction()
            #print "Random action: \t\t" + str(action)
            return action
        else:
            action = self.greedyAction(obs)
            #print "Nonrandom action: \t\t" + str(action)
            return action


    def init_args(self):
        self.BasisVectors = {}
        self.K_tilde = np.empty(shape=(0,0))
        self.a = np.empty(shape=(0,0))
        self.alpha_tilde = np.empty(shape=(0,0))
        self.C_tilde = np.empty(shape=(0,0))
        self.c_tilde = np.empty(shape=(0,0))
        self.kernel_vector = np.empty(shape=(0,0))
        self.kernel_matrix = np.empty(shape=(0,0))
        self.h = np.empty(shape=(0,0))
        self.d = 0
        self.s = float('inf')
        self.T = 0
        self.sarsa_epsilon = 0.1

    def agent_init(self,taskSpecString):

        TaskSpec = TaskSpecVRLGLUE3.TaskSpecParser(taskSpecString)
        if TaskSpec.valid:
            assert len(TaskSpec.getDoubleObservations())==4, "expecting 4 continuous observations"

            assert len(TaskSpec.getDoubleActions())==1, "expecting 1 continuous action"

        else:
            print "Task Spec could not be parsed: "+taskSpecString

        # Intialise all the variables
        self.state_spans = TaskSpec.getDoubleObservations()
        #self.reciproc_state_spans = [[1.0/(x[1]-x[0])] for x in self.state_spans]
        self.action_spans = TaskSpec.getDoubleActions()
        self.action_range = [self.action_spans[0][0] + x * (self.action_spans[0][1] - self.action_spans[0][0])/self.random_sample_size for x in range(0,self.random_sample_size)]
        print "Action spans: " + str(self.action_spans)

        self.init_args()
        pass

    def agent_start(self,observation):
        # We pick a random action to begin with, since we probably have no experience to generalise from
        action = Action()

        if len(self.BasisVectors) == 0:
            action.doubleArray = self.randomAction()
            self.BasisVectors[0] = self.stateActionPair(observation.doubleArray,action.doubleArray)
        else:
            action.doubleArray = self.epsilonGreedy(observation.doubleArray)
        if len(self.K_tilde) == 0:
            z = self.stateActionPair(observation.doubleArray,action.doubleArray)
            self.K_tilde = np.matrix([[1/self.RBFkernel(z,z)]])
        if self.a.size == 0:
            self.a = np.matrix([[1]])
        if self.alpha_tilde.size == 0:
            self.alpha_tilde = np.matrix([[0]])
        if self.C_tilde.size == 0:
            self.C_tilde = np.matrix([[0]])
        if self.c_tilde.size == 0:
            self.c_tilde = np.matrix([[0]])
        if self.kernel_vector.size == 0:
            self.kernel_vector = np.matrix([[0]])
        if self.kernel_matrix.size == 0:
            self.kernel_matrix = np.matrix([[0]])
        if self.h.size == 0:
            self.h = np.matrix([[0]])

        self.lastObservation = observation
        self.lastAction = action
        self.t = 0
        self.T = self.T+1
        self.sarsa_epsilon = 5.0/(10.0+self.T)

        returnAction = Action()
        returnAction.doubleArray = [action.doubleArray[0]*self.ACTION_SCALE]
        return returnAction

    def agent_step(self,reward, observation):
        # Since this is a sarsa-algorithm, we pick the action we're going to follow first
        action = Action()
        action.doubleArray = self.epsilonGreedy(observation.doubleArray)

        #print "PICKING ACTION TO BE " + str(action.doubleArray) + " FOR STATE " + str(observation.doubleArray)
        if not self.learningFrozen:
            self.updateQDeterministic(reward,observation,action)
        self.lastObservation = observation
        self.lastAction = action
        self.t += 1
        self.totalsteps += 1

        returnAction = Action()
        returnAction.doubleArray = [action.doubleArray[0]*self.ACTION_SCALE]
        return returnAction

    def updateQDeterministic(self,reward,observation,action):
        # Save old values
        #coef = GAMMA * self.sigma2 / self.s
        old_kernel_vector = np.atleast_2d(self.kernel_vector)
        old_a = copy.copy(self.a)
        #old_c_tilde = np.atleast_2d(self.c_tilde)
        #old_s = self.s
        #old_d = self.d

        # Update state-action pair and kernel values
        z = self.stateActionPair(observation.doubleArray,action.doubleArray)
        kernel_tt = self.RBFkernel(z,z)
        self.kernel_vector = self.fastKernelVector(observation.doubleArray,action.doubleArray)

        self.a = np.dot(self.K_tilde, self.kernel_vector)

        delta = (kernel_tt - np.dot(self.a.T, self.kernel_vector))[0,0]

        Delta_k = old_kernel_vector - self.GAMMA * self.kernel_vector

        self.d = reward - np.dot(Delta_k.T,self.alpha_tilde)

        if delta > self.nu:

            # Update K_tilde
            #print "K_tilde: " + str(self.K_tilde) + " and a: " + str(self.a)
            dkaa = self.K_tilde * delta + np.dot(self.a,self.a.T)
            self.K_tilde = np.concatenate((dkaa, -1 * self.a), axis = 1)
            extended_a = np.concatenate((-1 * self.a.T,[[1]]),axis = 1)
            self.K_tilde = np.concatenate((self.K_tilde,extended_a), axis = 0)
            #print "Shape of K_tilde: " + str(self.K_tilde.shape) + " and delta: " + str(delta)
            self.K_tilde =  self.K_tilde * (1/delta)

            # Extend a
            self.a = np.zeros((len(self.BasisVectors)+1,1))
            self.a[len(self.BasisVectors)] = [1]

            # Extend h
            self.h = np.concatenate((old_a,[[-self.GAMMA]]),axis=0)

            #print "Old a: " + str(old_a)
            #print "Old kernel vector: " + str(old_kernel_vector) + " and new kernel vector: " + str(self.kernel_vector) + " and kernel_tt " + str(kernel_tt)
            Delta_k_tt = (np.dot(old_a.T, (old_kernel_vector
                                        - 2*self.GAMMA*self.kernel_vector))
                                        + self.GAMMA*self.GAMMA * kernel_tt)


            self.c_tilde = self.h - np.concatenate((np.dot(self.C_tilde,Delta_k),[[0]]),axis=0)

            self.s = self.sigma2 + Delta_k_tt - np.dot(Delta_k.T, np.dot(self.C_tilde, Delta_k))


            self.alpha_tilde = np.concatenate((self.alpha_tilde,[[0]]), axis=0)

            self.C_tilde = np.concatenate((self.C_tilde,np.zeros((len(self.BasisVectors), 1))), axis=1)
            self.C_tilde = np.concatenate((self.C_tilde,np.zeros((1, len(self.BasisVectors)+1))), axis=0)


            self.BasisVectors[len(self.BasisVectors)] = self.stateActionPair(observation.doubleArray,action.doubleArray)

            self.kernel_vector = np.concatenate((self.kernel_vector,[[kernel_tt]]),axis=0)
        else:

            self.h = old_a - self.GAMMA*self.a
            Delta_k_tt = np.dot(self.h.T, Delta_k)
            self.c_tilde = self.h - np.dot(self.C_tilde, Delta_k)

            #print "s update deltak: " + str(Delta_k)
            #print "s update c tilde: " + str(self.c_tilde)
            #self.s = (1+GAMMA**2) * self.sigma2 - GAMMA * self.sigma2 * coef \
            #         + np.dot(Delta_k.T,(self.c_tilde + old_c_tilde * coef))
            self.s = self.sigma2 + Delta_k_tt - np.dot(Delta_k.T,np.dot(self.C_tilde,Delta_k))

        self.alpha_tilde = self.alpha_tilde + self.c_tilde * (self.d / self.s)
        #print "C_tilde update term 1: " + str(self.C_tilde)
        #print "C_tilde update term 2: " + str(np.multiply(np.dot(new_c, new_c.T), self.s))
        #print "C tilde update: " + str(self.C_tilde)
        #print "c tilde update: " + str(self.c_tilde)
        #print "s update: " + str(self.s)
        self.C_tilde = self.C_tilde + np.dot(self.c_tilde, self.c_tilde.T) * 1/self.s


    def updateQStochastic(self, reward, observation, action):
        # Save old values
        coef = self.GAMMA * self.sigma2 / self.s
        old_kernel_vector = self.kernel_vector
        old_a = np.atleast_2d(self.a)
        old_c_tilde = np.atleast_2d(self.c_tilde)
        old_s = self.s
        old_d = self.d

        # Update state-action pair and kernel values
        z = self.stateActionPair(observation.doubleArray,action.doubleArray)
        kernel_tt = self.RBFkernel(z,z)
        self.kernel_vector = self.fastKernelVector(observation.doubleArray, action.doubleArray)

        self.a = np.dot(self.K_tilde, self.kernel_vector)

        #print "Kernel vector: " + str(self.kernel_vector)
        delta = kernel_tt - np.dot(self.kernel_vector.T, self.a)[0][0]
        #print "Delta: " + str(delta)

        Delta_k = old_kernel_vector - self.GAMMA * self.kernel_vector
        
        self.d = coef * self.d + reward - np.dot(Delta_k.T,self.alpha_tilde)

        if delta > self.nu:


            # Update K_tilde
            dkaa = delta * self.K_tilde + np.dot(self.a,self.a.T)
            #print "Shape of K tilde before concat: " + str(self.K_tilde.shape)
            self.K_tilde = np.concatenate((dkaa, -1 * self.a), axis = 1)
            extended_a = np.concatenate((-1 * self.a.T,[[1]]),axis = 1)
            #print "Shape of extended a: " + str(extended_a.shape) + " and shape of K tilde: " + str(self.K_tilde.shape)
            self.K_tilde = np.concatenate((self.K_tilde,extended_a), axis = 0)
            self.K_tilde =  self.K_tilde * (1/delta[0,0])

            # Extend a
            self.a = np.zeros((len(self.BasisVectors)+1,1))
            self.a[len(self.BasisVectors)] = [1]

            # Extend h
            self.h = np.concatenate((old_a,[[-self.GAMMA]]),axis=0)


            Delta_k_tt = (np.dot(old_a.T, (old_kernel_vector
                                        - 2*self.GAMMA*self.kernel_vector))
                                        + self.GAMMA**2 * kernel_tt)

            self.c_tilde = np.concatenate((self.c_tilde,[[0]]),axis=0) * coef + self.h - np.concatenate((np.dot(self.C_tilde,Delta_k),[[0]]),axis=0)


            self.s = (1+self.GAMMA**2) * self.sigma2
            self.s += Delta_k_tt
            self.s -= np.dot(Delta_k.T,np.dot(self.C_tilde,Delta_k))
            self.s += 2 * coef * np.dot(old_c_tilde.T,Delta_k)
            self.s -= self.GAMMA * self.sigma2 * coef


            self.alpha_tilde = np.concatenate((self.alpha_tilde,[[0]]), axis=0)

            self.C_tilde = np.concatenate((self.C_tilde,np.zeros((len(self.BasisVectors), 1))), axis=1)
            self.C_tilde = np.concatenate((self.C_tilde,np.zeros((1, len(self.BasisVectors)+1))), axis=0)


            self.BasisVectors[len(self.BasisVectors)] = self.stateActionPair(observation.doubleArray,action.doubleArray)

            self.kernel_vector = np.concatenate((self.kernel_vector,[[kernel_tt]]),axis=0)



        else:
            #print "a-1: " + str(old_a)
            #print "GAMMA * self.a: " + str(GAMMA * self.a)
            self.h = old_a - self.GAMMA*self.a
            #Delta_k_tt = np.dot(self.h.T, Delta_k)
            self.c_tilde = self.c_tilde * coef + self.h - np.dot(self.C_tilde, Delta_k)

            #print "s update deltak: " + str(Delta_k)
            #print "s update c tilde: " + str(self.c_tilde)
            self.s = (1+self.GAMMA**2) * self.sigma2 - self.GAMMA * self.sigma2 * coef \
                     + np.dot(Delta_k.T,(self.c_tilde + old_c_tilde * coef))
            #self.s = Delta_k_tt - np.dot(Delta_k.T,np.dot(self.C_tilde,Delta_k))

        self.alpha_tilde = self.alpha_tilde + self.c_tilde * (self.d / self.s)
        #print "C_tilde update term 1: " + str(self.C_tilde)
        #print "C_tilde update term 2: " + str(np.multiply(np.dot(new_c, new_c.T), self.s))
        #print "C tilde update: " + str(self.C_tilde)
        #print "c tilde update: " + str(self.c_tilde)
        #print "s update: " + str(self.s)
        self.C_tilde = self.C_tilde + np.dot(self.c_tilde, self.c_tilde.T) * 1.0/self.s

    def updateQStochasticTerminal(self,reward):
        coef = self.GAMMA * self.sigma2 / self.s
        old_kernel_vector = np.atleast_2d(self.kernel_vector)
        old_a = np.atleast_2d(self.a)
        old_c_tilde = np.atleast_2d(self.c_tilde)
        old_s = self.s
        old_d = self.d

        Delta_k = old_kernel_vector
        Delta_k_tt = np.dot(old_a.T,old_kernel_vector)
        self.h = old_a
        self.c = old_c_tilde* coef + np.dot(self.C_tilde,Delta_k) - self.h
        self.s = self.sigma2 - self.GAMMA * self.sigma2 * coef - np.dot(Delta_k.T, self.c_tilde + old_c_tilde * coef)
        self.d = coef * old_d + np.dot(Delta_k.T,self.alpha_tilde) - reward
        self.kernel_vector = self.fastKernelVector(self.lastObservation.doubleArray, self.lastAction.doubleArray)
        self.a = np.dot(self.K_tilde, self.kernel_vector)

    def agent_end(self,reward):

        #self.updateQStochasticTerminal(reward)
        #kernel_tt = self.kernel(self.lastObservation.doubleArray,self.lastAction.doubleArray,self.lastObservation.doubleArray,self.lastAction.doubleArray)
        #self.kernel_vector = self.kernelVector(self.lastObservation.doubleArray,self.lastAction.doubleArray)
        # self.a = np.dot(self.K_tilde, self.kernel_vector)
        #

        #
        # self.h = old_a - GAMMA*self.a
        # #Delta_k_tt = np.dot(h.T, Delta_k)
        # self.c_tilde =  old_c_tilde * coef - np.dot(self.C_tilde, Delta_k) + self.h
        #
        # #print "s update deltak: " + str(Delta_k)
        # #print "s update c tilde: " + str(self.c_tilde)
        # self.s = (1+GAMMA**2) * self.sigma2 - GAMMA * self.sigma2 * coef \
        #          + np.dot(Delta_k.T,(self.c_tilde + old_c_tilde * coef))
        #
        # self.d = coef * old_d - np.dot(Delta_k.T,self.alpha_tilde) + reward
        # print "d was calculated as: " + str(self.d)
        # print "coef was: " + str(coef)
        # #print "alpha was: " + str(self.alpha_tilde)
        # #print "delta k was: " + str(Delta_k)
        #
        # self.alpha_tilde = self.alpha_tilde + self.c_tilde * (self.d / self.s)
        # #print "C_tilde update term 1: " + str(self.C_tilde)
        # #print "C_tilde update term 2: " + str(np.multiply(np.dot(new_c, new_c.T), self.s))
        # #print "C tilde update: " + str(self.C_tilde)
        # #print "c tilde update: " + str(self.c_tilde)
        # #print "s update: " + str(self.s)
        # self.C_tilde = self.C_tilde + np.dot(self.c_tilde, self.c_tilde.T) * 1/self.s
        print "number of steps before end was " + str(self.t)
        print "Size of dict is " + str(len(self.BasisVectors))
        print "Episode number was " + str(self.T)

        pass

    def agent_cleanup(self):
        pass

    def agent_message(self,inMessage):

        #Message Description
        # set_parameters KERNEL_BANDWIDTH DISCOUNT_FACTOR INTRINSIC_VARIANCE
        # Action: Sets the parameters to the given values
        #
        if inMessage.startswith("set_parameters"):
            self.init_args()
            splitString = inMessage.split(" ")
            self.KERNEL_BANDWIDTH = float(splitString[1])

            self.GAMMA = float(splitString[2])
            self.sigma2 = float(splitString[3])
            print "Set parameters to " + str(self.KERNEL_BANDWIDTH) + ", " + str(self.GAMMA) + ", " + str(self.sigma2)

        #Message Description
        # save_policy FILENAME
        # Action: Save current value function in binary format to
        # file called FILENAME
        #
        if inMessage.startswith("save policy"):
            splitString=inMessage.split(" ")
            self.save_value_function("GPTD Policy")
            print "Saved."
            return "message understood, saving policy"

         #	Message Description
        # 'freeze learning'
        # Action: Set flag to stop updating policy
        #
        if inMessage.startswith("freeze_learning"):
            self.learningFrozen=True
            print "Learning frozen"
            return "message understood, policy frozen"

        #	Message Description
        # unfreeze learning
        # Action: Set flag to resume updating policy
        #
        if inMessage.startswith("unfreeze_learning"):
            self.learningFrozen=False
            print "Learning unfrozen"
            return "message understood, policy unfrozen"

        #Message Description
        # freeze exploring
        # Action: Set flag to stop exploring (greedy actions only)
        #
        if inMessage.startswith("freeze_exploring"):
            #self.frozenEpsilon = self.epsilon
            #self.epsilon = 0.0
            self.explorationFrozen = True
            print "Exploration frozen"
            return "message understood, exploring frozen"

        #Message Description
        # unfreeze exploring
        # Action: Set flag to resume exploring (e-greedy actions)
        #
        if inMessage.startswith("unfreeze_exploring"):
            #self.epsilon = self.frozenEpsilon
            self.explorationFrozen = False
            print "Exploration unfrozen"
            return "message understood, exploring frozen"

    def save_value_function(self, fileName):
        theFile = open("AlphaTildeGPTD", "w")
        pickle.dump(self.alpha_tilde, theFile)
        theFile.close()

        theFile = open("BasisVectorsGPTD", "w")
        pickle.dump(self.BasisVectors, theFile)

        theFile.close()


if __name__=="__main__":
    AgentLoader.loadAgent(GPTDAgent())