import random
import sys
import copy
import pickle
from rlglue.agent.Agent import Agent
from rlglue.agent import AgentLoader as AgentLoader
from rlglue.types import Action
from rlglue.types import Observation
from rlglue.utils import TaskSpecVRLGLUE3
from random import Random
import numpy as np

class SarsaAgent(Agent):
    randGenerator=Random()
    lastAction=Action()
    lastObservation=Observation()
    sarsa_stepsize = 0.1
    sarsa_epsilon = 0.1
    sarsa_gamma = 1.0
    numStates = (10,20,100,40,10)
    observationRanges = None
    numActions = 10
    value_function = None


    policyFrozen=False
    exploringFrozen=False

    def agent_init(self,taskSpecString):
        TaskSpec = TaskSpecVRLGLUE3.TaskSpecParser(taskSpecString)
        if TaskSpec.valid:
            assert len(TaskSpec.getDoubleObservations())==4, "expecting 4 continuous observations"

            assert len(TaskSpec.getDoubleActions())==1, "expecting 1 continuous action"

        else:
            print "Task Spec could not be parsed: "+taskSpecString

        self.value_function = np.zeros(self.numStates)
        self.observationRanges = TaskSpec.getDoubleObservations()
        self.lastAction=Action()
        self.lastObservation=Observation()

    def egreedy(self, state):
        maxIndex=0
        a=1
        if not self.exploringFrozen and self.randGenerator.random()<self.sarsa_epsilon:
            return self.randGenerator.randint(0,self.numActions-1)

        return np.argmax(self.getValueFunctionRow(state[0],state[1],state[2],state[3]))

    def action_double(self,intAction):
        return -10.0 + intAction*2.0

    def action_int(self,doubleAction):
        return int(round((doubleAction + 10.0)/2.0))

    def getValueFunctionRow(self, x, xvel, theta, thetavel):

        xint = round((x-self.observationRanges[0][0])*self.numStates[0]/(self.observationRanges[0][1] - self.observationRanges[0][0]))
        xvelint = round((xvel-self.observationRanges[1][0])*self.numStates[1]/(self.observationRanges[1][1] - self.observationRanges[1][0]))
        thetaint = round((theta-self.observationRanges[2][0])*self.numStates[2]/(self.observationRanges[2][1] - self.observationRanges[2][0]))
        thetavelint = round((thetavel-self.observationRanges[3][0])*self.numStates[3]/(self.observationRanges[3][1] - self.observationRanges[3][0]))

        if xint < 0:
            xint = 0
        if xint > self.numStates[0]-1:
            xint = self.numStates[0]-1
        if xvelint < 0:
            xvelint = 0
        if xvelint > self.numStates[1]-1:
            xvelint = self.numStates[1]-1
        if thetaint > self.numStates[2]-1:
            thetaint = self.numStates[2]-1
        if thetaint < 0:
            thetaint = 0
        if thetavelint < 0:
            thetavelint = 0
        if thetavelint > self.numStates[3]-1:
            thetavelint = self.numStates[3]-1

        return self.value_function[xint][xvelint][thetaint][thetavelint]

    def agent_start(self,observation):
        theState=observation.doubleArray
        thisIntAction=self.egreedy(theState)
        returnAction=Action()
        returnAction.doubleArray=[self.action_double(thisIntAction)]

        self.lastAction=copy.deepcopy(returnAction)
        self.lastObservation=copy.deepcopy(observation)

        return returnAction


    def agent_step(self,reward, observation):
        newState=observation.doubleArray
        newIntAction=self.egreedy(newState)

        Q_sprime_aprime=self.getValueFunctionRow(newState[0],newState[1],newState[2],newState[3])[newIntAction]

        lastState=self.lastObservation.doubleArray

        lastAction=self.lastAction.doubleArray[0]

        Q_sa=self.getValueFunctionRow(lastState[0],lastState[1],lastState[2],lastState[3])[self.action_int(lastAction)]

        new_Q_sa=Q_sa + self.sarsa_stepsize * (reward + self.sarsa_gamma * Q_sprime_aprime - Q_sa)


        if not self.policyFrozen:
            self.getValueFunctionRow(newState[0],newState[1],newState[2],newState[3])[self.action_int(lastAction)]=new_Q_sa

        returnAction=Action()
        returnAction.doubleArray=[self.action_double(newIntAction)]

        self.lastAction=copy.deepcopy(returnAction)
        self.lastObservation=copy.deepcopy(observation)

        return returnAction

    def agent_end(self,reward):
        lastState=self.lastObservation.doubleArray
        lastAction=self.lastAction.doubleArray[0]

        Q_sa=self.getValueFunctionRow(lastState[0],lastState[1],lastState[2],lastState[3])[self.action_int(lastAction)]

        new_Q_sa=Q_sa + self.sarsa_stepsize * (reward - Q_sa)

        if not self.policyFrozen:
            self.getValueFunctionRow(lastState[0],lastState[1],lastState[2],lastState[3])[self.action_int(lastAction)]=new_Q_sa


    def agent_cleanup(self):
        pass

    def save_value_function(self, fileName):
        theFile = open(fileName, "w")
        pickle.dump(self.value_function, theFile)
        theFile.close()

    def load_value_function(self, fileName):
        theFile = open(fileName, "r")
        self.value_function=pickle.load(theFile)
        theFile.close()

    def agent_message(self,inMessage):

        #	Message Description
        # 'freeze learning'
        # Action: Set flag to stop updating policy
        #
        if inMessage.startswith("freeze learning"):
            self.policyFrozen=True
            return "message understood, policy frozen"

        #	Message Description
        # unfreeze learning
        # Action: Set flag to resume updating policy
        #
        if inMessage.startswith("unfreeze learning"):
            self.policyFrozen=False
            return "message understood, policy unfrozen"

        #Message Description
        # freeze exploring
        # Action: Set flag to stop exploring (greedy actions only)
        #
        if inMessage.startswith("freeze exploring"):
            self.exploringFrozen=True
            return "message understood, exploring frozen"

        #Message Description
        # unfreeze exploring
        # Action: Set flag to resume exploring (e-greedy actions)
        #
        if inMessage.startswith("unfreeze exploring"):
            self.exploringFrozen=False
            return "message understood, exploring frozen"

        #Message Description
        # save_policy FILENAME
        # Action: Save current value function in binary format to
        # file called FILENAME
        #
        if inMessage.startswith("save policy"):
            splitString=inMessage.split(" ");
            self.save_value_function(splitString[1]);
            print "Saved.";
            return "message understood, saving policy"

        #Message Description
        # load_policy FILENAME
        # Action: Load value function in binary format from
        # file called FILENAME
        #
        if inMessage.startswith("load policy"):
            splitString=inMessage.split(" ")
            self.load_value_function(splitString[1])
            print "Loaded."
            return "message understood, loading policy"

        return "SampleSarsaAgent(Python) does not understand your message."

if __name__=="__main__":
    AgentLoader.loadAgent(SarsaAgent())