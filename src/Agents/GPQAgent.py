__author__ = 'simon'


import random
import sys
import copy
import pickle
from rlglue.agent.Agent import Agent
from rlglue.agent import AgentLoader as AgentLoader
from rlglue.types import Action
from rlglue.types import Observation
from rlglue.utils import TaskSpecVRLGLUE3
import rlglue.RLGlue as RLGlue
from random import Random
import numpy as np
import scipy.linalg.blas as blas
import math
import time
from math import pi as PI


class GPQAgent(Agent):

    KERNEL_BANDWIDTH = 0.64
    KERNEL_PLACEMENT = 1.0
    BV_BUDGET = 100
    GAMMA = 0.60
    BETA_TOL = 0.5
    ACTION_SCALE = 10.0

    exploration_strategy = "optimistic"

    epsilon = 0.1
    frozenEpsilon = 0.0
    zeta = 1.0
    learningRate = 0.1
    explorationRate = 0.9

    lastObservation = None
    action = None

    action_spans = [(0, 0)]
    state_spans = [(0, 0), (0, 0), (0, 0), (0, 0)]
    #valid_actions = [-20.0, -10.0, -5.0, 0.0, 5.0, 10.0, 20.0]
    valid_actions = [-1.0, -0.5, -0.25, 0, 0.25, 0.5, 1.0]
    #valid_actions = [-1.0, 0, 1.0]

    BasisVectors = {}
    K_inv = np.array([[0]])
    alpha = np.array([[0]])
    s = np.array([[0]])
    C = np.array([[0]])
    kernel_vector = np.array([[0.0]])
    kernel_matrix = np.array([[0.0]])
    omega_2 = 0.1
    q = 1.0
    r = 1.0
    beta = 0.0

    steps = 1
    totalsteps = 1

    explorationFrozen = False
    learningFrozen = False

    def RBFkernel(self, z1, z2):
        val = self.KERNEL_PLACEMENT * math.exp(-(np.linalg.norm(np.array(z1)-np.array(z2))**2) / (2 * self.KERNEL_BANDWIDTH**2))
        return val

    # This function returns the element-wise product of the two kernel vectors above
    def kernelVector(self, obs, act):
        #print "Tried to calculate the kernel vector with observation " + str(obs) + " and action " + str(act)
        z = self.stateActionPair(obs, act)
        arr = [[self.RBFkernel(self.BasisVectors[x], z)] for x in self.BasisVectors]
        #print "kernel vector is " + str(arr)
        return np.atleast_2d(arr)

    def fastKernelVector(self, obs, act):
        z = np.atleast_2d(self.stateActionPair(obs, act))
        BVs = np.atleast_2d(self.BasisVectors.values())
        #print "Shape of BVs: " + str(BVs.shape) + ", shape of z: " + str(z.shape)
        squared_and_divided = np.square(np.linalg.norm(BVs-z,axis=1))/(2*self.KERNEL_BANDWIDTH**2)
        kernel_vector = np.atleast_2d(self.KERNEL_PLACEMENT * np.exp(-squared_and_divided))

        #print "Shape of fast kernel_vector: " + str(kernel_vector.shape)

        return kernel_vector.T

    # This function computes the kernel matrix from the basis vectors
    def kernelMatrix(self):
        n = len(self.BasisVectors)
        return [[self.RBFkernel(l,m) for l in range(0,n)] for m in range(0,n)]

    # This function calculates the approximation of the value of a given action based on accumulated experience
    def approxQ(self, obs, act):
        return np.dot(self.alpha.T,self.fastKernelVector(obs,act))

    # This function chooses a uniformly random action from the action span
    def randomAction(self):
        val = random.random()
        num_valid = len(self.valid_actions)
        chosen = int(val*num_valid)
        #print "Chose action number " + str(chosen) + " randomly"
        return [self.valid_actions[chosen]]
        #return [random.random()*(self.action_spans[0][1] - self.action_spans[0][0]) + self.action_spans[0][0]]

    def greedyAction(self,obs):
        Qs = np.array([self.approxQ(obs,[x]) for x in self.valid_actions])
        action_index = random.randint(0, len(self.valid_actions)-1)
        for i in range(0, len(self.valid_actions)):
            if Qs[i] > Qs[action_index]:
                action_index = i
        #print "Nonrandom action: \t\t" + str(self.valid_actions[action_index]) + " for timestep " + str(self.steps)
        #print "Picked action number " + str(action_index) + " from Q-array:"
        #print str(Qs)
        return np.array([self.valid_actions[action_index]])

    def greedyQ(self,obs):
        Qs = np.array([self.approxQ(obs,[x]) for x in self.valid_actions])
        return np.max(Qs)

    # Classical epsilon-greedy action selection
    def epsilonGreedy(self,obs):
        if random.random() < self.epsilon:
            action = self.randomAction()
            # print "Random action: \t\t" + str(action)
            return action
        else:
            action = self.greedyAction(obs)

            return action

    def Sigma(self, obs, act):
        z = self.stateActionPair(obs, act)
        k_vector = self.fastKernelVector(obs, act)
        Sigma = self.RBFkernel(z,z) + self.omega_2 + np.dot(k_vector.T, np.dot(self.C, k_vector))
        #Sigma[0][0] = abs(Sigma[0][0])
        if Sigma < 0: print "Sigma was negative! Sigma=" + str(Sigma) + " for omega^2=" + str(self.omega_2)
        #else: print "Sigma was: " + str(Sigma)
        return Sigma

    def upper_confidence_tail_greedy(self,obs):
        Qs = np.array([self.approxQ(obs,[x]) + 2*self.Sigma(obs,[x]) for x in self.valid_actions])
        action_index = random.randint(0, len(self.valid_actions)-1)
        for i in range(0, len(self.valid_actions)):
            if Qs[i] > Qs[action_index]:
                action_index = i
        #action_index = np.argmax(Qs)
        #print "Nonrandom action: \t\t" + str(self.valid_actions[action_index]) + " for timestep " + str(self.steps)
        #print "Picked action number " + str(action_index) + " from Q-array:"
        #print str(Qs)
        return np.array([self.valid_actions[action_index]])

    def stateActionPair(self,obs,action):
        # print "Tried to make a state-action pair from " + str(obs) + " and " +str(action)
        return np.concatenate((np.array(obs), np.array(action)),axis=1)

    def projectionInducedError(self,k_tau_plus,kernel_vector_tau_plus):
        return k_tau_plus - np.dot(kernel_vector_tau_plus.T, np.dot(self.K_inv, kernel_vector_tau_plus))

    def normalize_state(self,state):
         #return np.array([(state[i]-self.state_spans[i][0])/(self.state_spans[i][1] - self.state_spans[i][0]) for i in range(1,len(state))])
         return np.array([(2*state[i])/(self.state_spans[i][1] - self.state_spans[i][0]) for i in range(0,4)])
         #return np.array([(2*state[i])/(self.state_spans[i][1] - self.state_spans[i][0]) for i in range(2,4)])

    def agent_init(self,taskSpecString):

        TaskSpec = TaskSpecVRLGLUE3.TaskSpecParser(taskSpecString)
        if TaskSpec.valid:
            assert len(TaskSpec.getDoubleObservations())==4, "expecting 4 continuous observations"

            assert len(TaskSpec.getDoubleActions())==1, "expecting 1 continuous action"

        else:
            print "Task Spec could not be parsed: "+taskSpecString

        # Intialise all the variables
        self.state_spans = TaskSpec.getDoubleObservations()
        #self.reciproc_state_spans = [[1.0/(x[1]-x[0])] for x in self.state_spans]
        self.action_spans = TaskSpec.getDoubleActions()
        #print "Action spans: " + str(self.action_spans)

        self.init_args()

        pass

    def init_args(self):
        self.BasisVectors = {}
        self.K_inv = np.array([[0]])
        self.alpha = np.array([[0]])
        self.s = np.array([[0]])
        self.C = np.array([[0]])
        self.kernel_vector = np.array([[0.0]])
        self.kernel_matrix = np.array([[0.0]])
        self.q = 1.0
        self.r = 1.0

        self.steps = 1
        self.totalsteps = 1

    def agent_start(self,observation):
        # We pick a random action to begin with, since we probably have no experience to generalise from
        action = Action()
        observation.doubleArray = self.normalize_state(observation.doubleArray)

        if len(self.BasisVectors) == 0:
            action.doubleArray = self.randomAction()
            z_init = self.stateActionPair(observation.doubleArray,action.doubleArray)
            self.addBasisVector(z_init)
            self.K_inv = np.array([[1/self.RBFkernel(z_init,z_init)]])
            self.kernel_matrix = np.array([[self.RBFkernel(z_init,z_init)]])
        else:

            if (self.explorationFrozen):

                if self.exploration_strategy == "epsilon_greedy":
                    action.doubleArray = self.greedyAction(observation.doubleArray)
                elif self.exploration_strategy == "optimistic":
                    action.doubleArray = self.upper_confidence_tail_greedy(observation.doubleArray)
            else:

                if self.exploration_strategy == "epsilon_greedy":
                    action.doubleArray = self.epsilonGreedy(observation.doubleArray)
                elif self.exploration_strategy == "optimistic":
                    action.doubleArray = self.upper_confidence_tail_greedy(observation.doubleArray)


        self.lastObservation = observation
        self.action = action

        self.steps = 1
        self.totalsteps += 1

        self.z_tau_plus = self.stateActionPair(observation.doubleArray,action.doubleArray)
        returnAction = Action()
        returnAction.doubleArray = [self.action.doubleArray[0]*self.ACTION_SCALE]
        #self.zeta = 0.5/self.totalsteps**self.learningRate
        print "Starting with Kernel placement: " + str(self.KERNEL_PLACEMENT) + ", kernel bandwidth: " + str(self.KERNEL_BANDWIDTH) + ", gamma: " + str(self.GAMMA) + ", exploration rate: " + str(self.explorationRate) + " and learning rate: " + str(self.learningRate)

        return returnAction

    def agent_step(self,reward, observation):

        #if reward < 0: print "Received negative reward!"
        #print "Unnormalized observation: " + str(observation.doubleArray)
        observation.doubleArray = self.normalize_state(observation.doubleArray)
        #print "Normalized observation: " + str(observation.doubleArray)

        if not self.learningFrozen:
            # Greedy action and corresponding y-value
            y_tau = 0.0
            y_tau_time_start = time.clock()
            if self.exploration_strategy == "epsilon_greedy":
                #b = self.greedyAction(observation.doubleArray)
                #y_tau = reward + self.GAMMA * self.approxQ(observation.doubleArray, b)
                y_tau = reward + self.GAMMA * self.greedyQ(observation.doubleArray)

            elif self.exploration_strategy == "optimistic":
                b = self.upper_confidence_tail_greedy(observation.doubleArray)
                y_tau = reward + self.GAMMA * (self.approxQ(observation.doubleArray, b) + 2*self.Sigma(observation.doubleArray,b))

            y_tau_time_end = time.clock()

            #print "y tau time: " + str(y_tau_time_end - y_tau_time_start)

            #print "Got projected reward to be: " + str(y_tau) + " For given reward " + str(reward)
            # State-action pairs
            self.z_tau = self.z_tau_plus
            self.z_tau_plus = self.stateActionPair(self.lastObservation.doubleArray,self.action.doubleArray)
            #z_tau_plus = self.stateActionPair(observation.doubleArray,b)

            # Update kernel vectors and kernel values
            self.old_kernel_vector = self.kernel_vector

            #kernel_vector_time_start = time.clock()
            #kernel_vector = self.kernelVector(self.lastObservation.doubleArray,self.action.doubleArray)
            #kernel_vector_time_end = time.clock()
            fast_kernel_vector_time_start = time.clock()
            self.kernel_vector = self.fastKernelVector(self.lastObservation.doubleArray,self.action.doubleArray)
            fast_kernel_vector_time_end = time.clock()

            #print "kernel_vector time: " + str(kernel_vector_time_end-kernel_vector_time_start) + ", fast_kernel_vector time: " + str(fast_kernel_vector_time_end-fast_kernel_vector_time_start)
            #print "difference between kernel vectors: " + str(kernel_vector-self.kernel_vector)

            #kernel_vector_tau_plus = self.kernelVector(observation.doubleArray,b)
            k_tau = self.RBFkernel(self.z_tau,self.z_tau)
            k_tau_plus = self.RBFkernel(self.z_tau_plus,self.z_tau_plus)

            # Update omega, q and r to be used in updates of parameters
            #self.update_omega()
            q_time_start = time.clock()
            self.update_q(y_tau,k_tau)
            q_time_end = time.clock()
            r_time_start = time.clock()
            self.update_r(k_tau,self.kernel_vector)
            r_time_end = time.clock()

            #print "q time: " + str(q_time_end - q_time_start) + ", r time: " + str(r_time_end - r_time_start)

            # Linear independence test
            beta_time_start = time.clock()
            self.beta = self.projectionInducedError(k_tau_plus,self.kernel_vector)
            beta_time_end = time.clock()

            #print "beta time: " + str(beta_time_end - beta_time_start)

            #print "Beta tolerance was: " + str(self.beta)


            if self.beta > self.BETA_TOL: # and len(self.BasisVectors) <= BV_BUDGET:
                # If the error is too large, add a new basis vector and update the parameters
                self.addBasisVector(self.z_tau_plus)

                #self.kernel_matrix = self.kernelMatrix()

                #print "Shape of kernel_matrix: " + str(self.kernel_matrix.shape) + ", and shape of kernel_vector: " + str(self.kernel_vector.shape)

                fast_kernel_mat_start = time.clock()
                self.kernel_matrix = np.concatenate((self.kernel_matrix,self.kernel_vector),axis=1)
                self.kernel_matrix = np.concatenate((self.kernel_matrix,np.concatenate((self.kernel_vector,[[k_tau_plus]]),axis=0).T),axis=0)
                fast_kernel_mat_end = time.clock()
                #print "kernel matrix time is now: " + str(fast_kernel_mat_end-fast_kernel_mat_start)


                # Update params
                s_time_start = time.clock()
                self.update_s()
                s_time_end = time.clock()

                C_time_start = time.clock()
                self.update_C()
                C_time_end = time.clock()

                alpha_time_start = time.clock()
                self.update_alpha()
                alpha_time_end = time.clock()

                K_inv_time_start = time.clock()
                self.update_K_inv()
                K_inv_time_end = time.clock()

                #print "s time: " + str(s_time_end - s_time_start) + ", C time: " + str(C_time_end - C_time_start) + ", alpha time: " + str(alpha_time_end - alpha_time_start) + ", K inv time: " + str(K_inv_time_end - K_inv_time_start)

                #self.K_inv = (self.K_inv + self.K_inv.T)/2.0
            else:
                # If we don't add more basis vectors we perform reduced updates
                self.update_s_reduc()
                self.update_C_reduc()
                self.update_alpha_reduc()

            #self.zeta = 0.5/(self.totalsteps**self.learningRate)
            self.totalsteps += 1
            #self.C = (self.C + self.C.T)/2.0

            if len(self.BasisVectors) > self.BV_BUDGET:
                del_time_start = time.clock()
                self.delMinBasisVector()
                del_time_end = time.clock()
                #print "delete time: " + str(del_time_end - del_time_start)

                #self.kernel_matrix = self.kernelMatrix()
                pass

        # Make ready for next iteration
        self.lastObservation = observation
        self.action = Action()
        self.steps += 1

        if not self.explorationFrozen:
            self.epsilon = 1.0/(self.totalsteps**self.explorationRate)

            if self.exploration_strategy == "epsilon_greedy":
                self.action.doubleArray = self.epsilonGreedy(observation.doubleArray)
            elif self.exploration_strategy == "optimistic":
                self.action.doubleArray = self.upper_confidence_tail_greedy(observation.doubleArray)
        else:
            if self.exploration_strategy == "epsilon_greedy":
                self.action.doubleArray = self.greedyAction(observation.doubleArray)
            elif self.exploration_strategy == "optimistic":
                self.action.doubleArray = self.upper_confidence_tail_greedy(observation.doubleArray)


        #self.action.doubleArray[0] *= ACTION_SCALE
        #return self.action

        self.kernel_vector = self.fastKernelVector(self.lastObservation.doubleArray, self.action.doubleArray)

        returnAction = Action()
        returnAction.doubleArray = [self.action.doubleArray[0]*self.ACTION_SCALE]


        return returnAction


    def addBasisVector(self,z):
        self.BasisVectors[len(self.BasisVectors)] = z

    def delMinBasisVector(self):
        min_score = self.basisVectorScore(0)
        del_index = 0
        for i in range(1,len(self.BasisVectors)):
            temp = self.basisVectorScore(i)
            if temp < min_score:
                min_score = temp
                del_index = i

        self.deleteBasisVector(del_index)

    def basisVectorScore(self,i):
        return abs(self.alpha[i])/self.K_inv[i,i]

    def deleteBasisVector(self,i):

        t1_start = time.clock()
        t_plus = len(self.BasisVectors)-1
        #print "Deleting basis vector number " + str(i) + " with t_plus equal to " + str(t_plus)
        swap_rows(self.K_inv, i, t_plus)
        swap_cols(self.K_inv, i, t_plus)
        t1_end = time.clock()

        t2_start = time.clock()
        K_inv_star = np.atleast_2d(self.K_inv[t_plus,:]).T
        #print "K_inv_star before delete of " + str(i) + ": " + str(K_inv_star) + " taken from K_inv: " +str(self.K_inv)
        K_inv_star = np.delete(K_inv_star, t_plus, 0)
        #print "K_inv_star after delete is " + str(K_inv_star)
        #assert np.size(K_inv_star,axis=0) == t_plus
        k_inv_star = self.K_inv[t_plus,t_plus]

        self.K_inv = np.delete(self.K_inv, t_plus, axis=1)
        self.K_inv = np.delete(self.K_inv, t_plus, axis=0)
        #self.K_inv = remove_ij(self.K_inv, t_plus, t_plus)
        t2_end = time.clock()
        #assert np.size(self.K_inv,axis=0) == t_plus
        #assert np.size(self.K_inv,axis=1) == t_plus

        t3_start = time.clock()
        swap_rows(self.C, i, t_plus)
        swap_cols(self.C, i, t_plus)
        t3_end = time.clock()

        C_star = np.atleast_2d(self.C[:,t_plus]).T
        C_star = np.delete(C_star, t_plus, 0)
        #assert np.size(C_star, axis=0) == t_plus,1
        c_star = self.C[t_plus,t_plus]
        self.C = np.delete(self.C, t_plus, axis=1)
        self.C = np.delete(self.C, t_plus, axis=0)

        #self.C = remove_ij(self.C, t_plus, t_plus)
        #assert np.shape(self.C) == (t_plus,t_plus)


        t4_start = time.clock()
        self.BasisVectors[i] = self.BasisVectors[t_plus]
        #print "Deleting basis vector: " + str(self.BasisVectors[t_plus])
        del self.BasisVectors[t_plus]
        t4_end = time.clock()
        #assert len(self.BasisVectors) == t_plus
        t11_start = time.clock()
        #self.kernel_matrix = self.kernelMatrix()
        #print "Shape of kernel matrix before removal: " + str(self.kernel_matrix.shape)
        swap_rows(self.kernel_matrix, i, t_plus)
        swap_cols(self.kernel_matrix, i, t_plus)
        self.kernel_matrix = remove_ij(self.kernel_matrix, t_plus, t_plus)
        #np.delete(self.kernel_matrix, t_plus, axis=1)
        #np.delete(self.kernel_matrix, t_plus, axis=0)
        #print "Shape of kernel matrix after removal: " + str(self.kernel_matrix.shape)
        t11_end = time.clock()

        t5_start = time.clock()
        alpha_star = self.alpha[i][0]
        self.alpha[i] = self.alpha[t_plus]
        self.alpha = np.delete(self.alpha, t_plus, axis=0)
        t5_end = time.clock()
        #assert np.size(self.alpha, axis=0) == t_plus
        #print "Shape of alpha after delete: " + str(np.shape(self.alpha))

        #self.kernel_vector[i] = self.kernel_vector[t_plus]
        #np.delete(self.kernel_vector, t_plus, axis=1)
        #assert np.shape(self.kernel_vector) == (t_plus,1)

        #self.kernel_matrix = self.kernelMatrix()
        #assert np.shape(self.kernel_matrix) == (t_plus,t_plus)

        t6_start = time.clock()
        self.s[i] = self.s[t_plus]
        self.s = np.delete(self.s, t_plus, axis=0)
        t6_end = time.clock()
        #assert np.size(self.s, axis=0) == t_plus

        t7_start = time.clock()
        QQ = blas.dgemm(alpha=1.0, a=K_inv_star, b=K_inv_star, trans_b=True)
        #QQ = np.dot(K_inv_star,K_inv_star.T)
        t7_end = time.clock()

        t8_start = time.clock()
        self.K_inv = self.K_inv - QQ * 1.0/k_inv_star
        t8_end = time.clock()
        #self.K_inv = (self.K_inv + self.K_inv.T) * 1.0/2.0

        #if not np.allclose(np.dot(self.kernel_matrix,self.K_inv), np.eye(len(self.BasisVectors)), rtol=1e-03, atol=1e-03):
            #print "K_inv was wrong after deletion!"
            #self.K_inv = np.linalg.inv(self.kernel_matrix)


        t9_start = time.clock()
        #KC = np.dot(K_inv_star,C_star.T)
        KC = blas.dgemm(alpha=1.0, a=K_inv_star, b=C_star, trans_b=True)
        self.C = self.C + c_star * QQ* 1.0/(k_inv_star**2) \
                 - 1.0/k_inv_star *(KC + KC.T)
        t9_end = time.clock()
        #self.C = (self.C + self.C.T) *1.0/2.0

        #C_inv = self.kernel_matrix + np.eye(len(self.BasisVectors)) * self.omega_2
        #if not np.allclose(np.dot(-self.C,C_inv), np.eye(len(self.BasisVectors)), rtol=1e-03, atol=1e-03):
            #print "C was wrong after delete!"
            #self.C = -1.0 * np.linalg.inv(self.kernel_matrix + np.eye(len(self.BasisVectors)) * self.omega_2)

        t10_start = time.clock()
        tempQ = K_inv_star/k_inv_star
        self.alpha = self.alpha - tempQ * alpha_star
        t10_end = time.clock()

        #print "Time for deletions: t1: " + str(t1_end-t1_start) + ", t2: " + str(t2_end-t2_start) + ", t3: " + str(t3_end-t3_start) + ", t4: " + str(t4_end - t4_start) + ", t5: " + str(t5_end - t5_start) + \
        #      ", t6: " + str(t6_end-t6_start) + ", t7: " + str(t7_end-t7_start) + ", t8: " + str(t8_end-t8_start) + ", t9: " + str(t9_end-t9_start) + ", t10: " + str(t10_end-t10_start) + ", t11: " + str(t11_end-t11_start)
        #self.C = self.C + np.dot(tempQ*c_star, tempQ.T)
        #tempQ = np.dot(tempQ, C_star.T)
        #self.C = self.C - tempQ - tempQ.T
        #self.C = (self.C + self.C.T) *1.0/2.0


        #print "Stuff going into calculating alpha: K_inv_star: \n" + str(K_inv_star) + ", alpha_star: " + str(alpha_star) + ", k_inv_star: " + str(k_inv_star)
        #print "Alpha before delete: \n" + str(self.alpha)

        #self.alpha = self.alpha * 1.0/np.linalg.norm(self.alpha,2)
        #print "Alpha after delete: \n" + str(self.alpha)
        #print "Shape of alpha after update: " + str(np.shape(self.alpha))


    def update_omega(self):
        #print "Norm of kernel matrix is " + str(np.linalg.norm(self.kernelMatrix(),np.inf))
        pass
        #self.omega_2 = 2* (np.linalg.norm(self.kernelMatrix(),np.inf) - 1.0)

    def update_q(self,y,k_tau):
        nominator = (y - np.dot(self.alpha.T, self.old_kernel_vector))
        denominator = self.omega_2 + np.dot(self.kernel_vector.T, np.dot(self.C, self.kernel_vector)) + k_tau

        self.q = nominator / denominator
        self.q = self.zeta * self.q
        #print "Updated q to " + str(self.q) + " with nominator: " + str(nominator) + " and denominator: " + str(denominator) + " and y: " + str(y) + " and alpha dot k: " + str(np.dot(self.alpha.T, self.old_kernel_vector))

    def update_r(self,k_tau,kernel_vector_tau_plus):
        self.r = -1.0/(self.omega_2 + np.dot(kernel_vector_tau_plus.T, np.dot(self.C, kernel_vector_tau_plus))
                  + k_tau)

        self.r = self.zeta*self.r


    def update_s(self):
        e = self.unit_vector(len(self.BasisVectors))
        #print "Tried to update s with C: " + str(self.C) + ", kernel vector: " + str(self.kernel_vector) + " and unit vector: " + str(e)
        Temp = self.extend_column_vector(np.dot(self.C,self.kernel_vector))
        self.s = Temp + e


    def update_C(self):
        #print "Updating C"
        self.C = self.extend_matrix(self.C, len(self.BasisVectors), len(self.BasisVectors)) \
               + np.dot(self.s,self.s.T) * self.r
        self.C = (self.C + self.C.T) *1.0/2.0

        #C_inv = self.kernel_matrix + np.eye(len(self.BasisVectors)) * self.omega_2
        #if not np.allclose(np.dot(-self.C,C_inv), np.eye(len(self.BasisVectors)), rtol=1e-03, atol=1e-03):
            #print "C was wrong after update!"
            #self.C = -1.0 * np.linalg.inv(self.kernel_matrix + np.eye(len(self.BasisVectors)) * self.omega_2)


    def update_alpha(self):
        #print "Tried to update alpha with s: " + str(self.s) + ", q: " + str(self.q) + " and alpha: " + str(self.alpha)
        self.alpha = self.extend_column_vector(self.alpha) + self.s * self.q
        #print "Resulted in alpha: " + str(self.alpha)

    def update_K_inv(self):
        #print "Shape of unit vector: " + str(np.shape(self.unit_vector(len(self.BasisVectors)))) + " and shape of kernel vector t plus: " + str(np.shape((kernel_vector_tau_plus))) + " and n: " + str(len(self.BasisVectors))
        #print "Shape of kernel matrix: " + str(np.shape(self.kernelMatrix()))
        e_hat = np.dot(self.K_inv, self.kernel_vector)
        #print "Shape of e_hat: " + str(np.shape(e_hat)) + " shape of K_inv: " + str(np.shape(self.K_inv))
        e_diff = self.extend_column_vector(e_hat) - self.unit_vector(len(self.BasisVectors))
        #print "e_diff = " + str(e_diff)
        #gamma_tau_plus = k_tau_plus - np.dot(kernel_vector_tau_plus.T,np.dot(self.K_inv,kernel_vector_tau_plus))
        #print "k tau plus: " + str(k_tau_plus) + "gamma tau plus: " + str(gamma_tau_plus) + " kernel vector product: " + str(np.dot(kernel_vector_tau_plus.T,np.dot(self.K_inv,kernel_vector_tau_plus)))
        self.K_inv = self.extend_matrix(self.K_inv,len(self.BasisVectors),len(self.BasisVectors)) \
                   + np.dot(e_diff,e_diff.T) * (1.0/self.beta)
        self.K_inv = (self.K_inv + self.K_inv.T)/2.0
        #if not np.allclose(np.dot(self.kernel_matrix,self.K_inv), np.eye(len(self.BasisVectors)), rtol=1e-03, atol=1e-03):
            #print "K_inv was wrong after update!"
            #self.K_inv = np.linalg.inv(self.kernel_matrix)

    def update_s_reduc(self):
        e_hat = np.dot(self.K_inv,self.kernel_vector)
        Temp = np.dot(self.C,self.kernel_vector)
        self.s = Temp + e_hat

    def update_C_reduc(self):
        #self.C = self.C + self.zeta * self.r * np.dot(self.s,self.s.T)
        self.C = self.C + np.dot(self.s,self.s.T) * self.r
        self.C = (self.C + self.C.T) *1.0/2.0
        #C_inv = self.kernel_matrix + np.eye(len(self.BasisVectors)) * self.omega_2
        #if not np.allclose(np.dot(-self.C,C_inv), np.eye(len(self.BasisVectors)), rtol=1e-03, atol=1e-03):
            #print "C was wrong after reduced update!"
            #self.C = -1.0 * np.linalg.inv(self.kernel_matrix + np.eye(len(self.BasisVectors)) * self.omega_2)


    def update_alpha_reduc(self):
        #print "Updated alpha redux with alpha: " + str(self.alpha) + " q: " + str(self.q) + " and s: " + str(self.s)
        #self.alpha = self.alpha + self.zeta * self.q * self.s
        self.alpha = self.alpha + self.s * self.q
        #print "Resulted in alpha: " + str(self.alpha)


    # Helper functions for vectors and matrices
    def unit_vector(self,t):
        e = np.zeros((t,1))
        e[t-1][0] = 1.0
        #print "Unit vector made: " + str(e)
        return e

    def extend_column_vector(self,vector):
        #print "Tried to extend vector: " + str(vector)
        return np.concatenate((vector,[[0]]),axis=0)

    def extend_matrix(self,matrix,new_width,new_height):
        column_zeros = np.zeros((new_height-1,1))
        row_zeros = np.zeros((1,new_width))
        #print "Matrix to be extended: " + str(matrix)
        #print "Row zeros: " + str(row_zeros)
        #print "Column zeros: " + str(column_zeros)
        temp = np.concatenate((matrix,column_zeros),axis=1)
        #print "Matrix after first extension: " + str(temp)
        return np.concatenate((temp,row_zeros),axis=0)



    def agent_end(self, reward):
        if not self.learningFrozen:
            y_tau = reward
            #b = self.upper_confidence_tail_greedy(observation.doubleArray)
            #y_tau = reward + GAMMA * (self.approxQ(observation.doubleArray, b) + 2*self.Sigma(observation.doubleArray,b))

            # State-action pairs
            self.z_tau = self.z_tau_plus
            self.z_tau_plus = self.stateActionPair(self.lastObservation.doubleArray,self.action.doubleArray)
            #z_tau_plus = self.stateActionPair(observation.doubleArray,b)

            # Update kernel vectors and kernel values
            self.old_kernel_vector = self.kernel_vector
            self.kernel_vector = self.fastKernelVector(self.lastObservation.doubleArray,self.action.doubleArray)
            #kernel_vector_tau_plus = self.kernelVector(observation.doubleArray,b)
            k_tau = self.RBFkernel(self.z_tau,self.z_tau)
            k_tau_plus = self.RBFkernel(self.z_tau_plus,self.z_tau_plus)

            # Update omega, q and r to be used in updates of parameters
            #self.update_omega()
            self.update_q(y_tau,k_tau)
            self.update_r(k_tau,self.kernel_vector)

            # Linear independence test
            self.beta = self.projectionInducedError(k_tau_plus,self.kernel_vector)
            #print "Beta tolerance was: " + str(beta)

            if self.beta > self.BETA_TOL: # and len(self.BasisVectors) <= BV_BUDGET:
                # If the error is too large, add a new basis vector and update the parameters
                self.addBasisVector(self.z_tau_plus)

                #self.kernel_matrix = self.kernelMatrix()
                self.kernel_matrix = np.concatenate((self.kernel_matrix,self.kernel_vector),axis=1)
                self.kernel_matrix = np.concatenate((self.kernel_matrix,np.concatenate((self.kernel_vector,[[k_tau_plus]]),axis=0).T),axis=0)

                # Update params
                self.update_s()
                self.update_C()
                self.update_alpha()
                self.update_K_inv()
                #self.K_inv = (self.K_inv + self.K_inv.T)/2.0
            else:
                # If we don't add more basis vectors we perform reduced updates
                # Update omega
                self.update_s_reduc()
                self.update_C_reduc()
                self.update_alpha_reduc()

            #self.zeta = 0.5/(self.totalsteps**self.learningRate)
            self.totalsteps += 1
            #self.C = (self.C + self.C.T)/2.0

            if len(self.BasisVectors) > self.BV_BUDGET:
                self.delMinBasisVector()
                pass

            self.kernel_vector = self.fastKernelVector(self.lastObservation.doubleArray, self.action.doubleArray)

        #print "Kernel matrix is " + str(self.kernel_matrix)
        #print "Basis vectors are " +
        # str(self.BasisVectors.items())
        #print "Alpha was " + str(self.alpha)
        #print "r was " + str(self.r)
        #print "q was " + str(self.q)
        print "Steps before end was " + str(self.steps)
        print "Size of dictionary was " + str(len(self.BasisVectors))
        if (math.isnan(self.r)):
            print "r is NaN!"
            self.r = 1.0
        if (math.isnan(self.q)):
            print "q is NaN!"
            self.q = 1.0
        pass

    def agent_cleanup(self):
        pass

    def agent_message(self,inMessage):

        #Message Description
        # set_parameters KERNEL_BANDWIDTH KERNEL_PLACEMENT EXPLORATION_RATE DISCOUNT_FACTOR LEARNING_RATE
        # Action: Sets the parameters to the given values
        #
        if inMessage.startswith("set_parameters"):
            self.init_args()
            splitString = inMessage.split(" ")
            self.KERNEL_BANDWIDTH = float(splitString[1])
            self.KERNEL_PLACEMENT = float(splitString[2])
            self.explorationRate = float(splitString[3])
            self.GAMMA = float(splitString[4])
            #self.zeta = float(splitString[5])
            print "Set parameters to " + str(self.KERNEL_BANDWIDTH) + ", " + str(self.KERNEL_PLACEMENT) + ", " + str(self.explorationRate) + ", " + str(self.GAMMA) + ", " + str(self.zeta)

        #Message Description
        # save_policy FILENAME
        # Action: Save current value function in binary format to
        # file with suffix FILENAME
        #
        if inMessage.startswith("save_policy"):
            splitString=inMessage.split(" ")
            self.save_value_function(splitString[1])
            print "Saved."
            return "message understood, saving policy"

         #	Message Description
        # 'freeze learning'
        # Action: Set flag to stop updating policy
        #
        if inMessage.startswith("freeze_learning"):
            self.learningFrozen=True
            print "Learning frozen"
            return "message understood, policy frozen"

        #	Message Description
        # unfreeze learning
        # Action: Set flag to resume updating policy
        #
        if inMessage.startswith("unfreeze_learning"):
            self.learningFrozen=False
            print "Learning unfrozen"
            return "message understood, policy unfrozen"

        #Message Description
        # freeze exploring
        # Action: Set flag to stop exploring (greedy actions only)
        #
        if inMessage.startswith("freeze_exploring"):
            #self.frozenEpsilon = self.epsilon
            #self.epsilon = 0.0
            self.explorationFrozen = True
            print "Exploration frozen"
            return "message understood, exploring frozen"

        #Message Description
        # unfreeze exploring
        # Action: Set flag to resume exploring (e-greedy actions)
        #
        if inMessage.startswith("unfreeze_exploring"):
            #self.epsilon = self.frozenEpsilon
            self.explorationFrozen = False
            print "Exploration unfrozen"
            return "message understood, exploring frozen"

    def save_value_function(self, fileName):
        theFile = open("AlphaTildeGPQ " + fileName, "w")
        pickle.dump(self.alpha, theFile)
        theFile.close()

        theFile = open("BasisVectorsGPQ " + fileName, "w")
        pickle.dump(self.BasisVectors, theFile)

        theFile.close()

def swap_cols(arr, frm, to):
    if frm != to:
        arr[:,[frm, to]] = arr[:,[to, frm]]

def swap_rows(arr, frm, to):
    if frm != to:
        arr[[frm, to],:] = arr[[to, frm],:]

def remove_ij(x, i, j):
    # Row i and column j divide the array into 4 quadrants
    y = x[:-1,:-1]
    y[:i,j:] = x[:i,j+1:]
    y[i:,:j] = x[i+1:,:j]
    y[i:,j:] = x[i+1:,j+1:]
    return y

if __name__=="__main__":
    AgentLoader.loadAgent(GPQAgent())