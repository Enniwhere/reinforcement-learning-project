__author__ = 'simon'



import random
import sys
import copy
import pickle
from rlglue.agent.Agent import Agent
from rlglue.agent import AgentLoader as AgentLoader
from rlglue.types import Action
from rlglue.types import Observation
from rlglue.utils import TaskSpecVRLGLUE3
from random import Random
import numpy as np
import math
from math import pi as PI


N_BOXES = 162
ALPHA = 1000
BETA = 0.5
GAMMA = 0.95
LAMBDAw = 0.9
LAMBDAv = 0.8

class SuttonAgent(Agent):

    w = None
    v = None
    e = None
    xbar = None
    lastObservation = None
    num_steps = 0

    def prob_push_right(self,s):
        return 1.0 / (1.0 + math.exp(-max(-50.0, min(s, 50.0))))

    def get_action(self,box):

        choice = random.random() < self.prob_push_right(self.w[box])
        #print "Choice of action was " + str(choice) + " which was chosen with probability " + str(self.prob_push_right(self.w[box])) + "\n"
        if not choice:
            return -10.0
        else:
            return 10.0

    def agent_init(self,taskSpecString):
        TaskSpec = TaskSpecVRLGLUE3.TaskSpecParser(taskSpecString)
        if TaskSpec.valid:
            assert len(TaskSpec.getDoubleObservations())==4, "expecting 4 continuous observations"

            assert len(TaskSpec.getDoubleActions())==1, "expecting 1 continuous action"

        else:
            print "Task Spec could not be parsed: "+taskSpecString

        self.w = np.zeros(N_BOXES)
        self.v = np.zeros(N_BOXES)
        self.xbar = np.zeros(N_BOXES)
        self.e = np.zeros(N_BOXES)
        self.lastObservation=Observation()



    def agent_start(self,observation):
        theState=observation.doubleArray
        self.lastObservation = observation
        self.num_steps = 0

        box = self.getBox(theState[0],theState[1],theState[2],theState[3])
        self.oldp = self.v[box]
        returnAction=Action()
        returnAction.doubleArray = [self.get_action(box)]

        return returnAction


    def agent_step(self,reward, observation):


        self.num_steps += 1
        newState=observation.doubleArray
        box = self.getBox(newState[0],newState[1],newState[2],newState[3])

        rhat = reward + GAMMA * self.v[box] - self.oldp
        #print "reward was " + str(reward)
        self.w += ALPHA * rhat * self.e
        self.v += BETA * rhat * self.xbar

        self.e = self.e * LAMBDAw
        self.xbar = self.xbar * LAMBDAv

        returnAction=Action()
        returnAction.doubleArray = [self.get_action(box)]
        self.e[box] += (1.0 - LAMBDAw) * (self.get_action(box)-0.5)
        self.xbar[box] += (1.0 - LAMBDAv)
        self.oldp = self.v[box]
        self.lastObservation = observation
        #if (self.num_steps >= 999):
        #    print "Completed an episode!"
        return returnAction

    def agent_end(self,reward):
        lastState=self.lastObservation.doubleArray

        box = self.getBox(lastState[0],lastState[1],lastState[2],lastState[3])
        p = self.v[box]
        if reward == 0:
            p = 0
            self.e = np.zeros(N_BOXES)
            self.xbar = np.zeros(N_BOXES)
        else:
            self.e *= LAMBDAw
            self.xbar *= LAMBDAv

        rhat = reward + GAMMA * p - self.oldp

        self.w += ALPHA * rhat * self.e
        self.v += BETA * rhat * self.xbar

        print "number of steps before end was " + str(self.num_steps)


    def agent_cleanup(self):
        pass

    def save_value_function(self, fileName):
        theFile = open(fileName, "w")
        pickle.dump(self.value_function, theFile)
        theFile.close()

    def load_value_function(self, fileName):
        theFile = open(fileName, "r")
        self.value_function=pickle.load(theFile)
        theFile.close()


    def getBox(self,x,xvel,theta,thetavel):
        box = 0
        one_degree = 2.0*PI/360.0

        if x < -0.8:
            box = 0
        elif x < 0.8:
            box = 1
        else:
            box = 2

        if xvel < -0.5:
            pass
        elif xvel < 0.5:
            box += 3
        else:
            box += 6

        if theta < -6*one_degree:
            pass
        elif theta < -one_degree:
            box += 9
        elif theta < 0:
            box += 18
        elif theta < one_degree:
            box += 27
        elif theta < 6*one_degree:
            box += 36
        else:
            box += 45

        if thetavel < -50*one_degree:
            pass
        elif thetavel < 50*one_degree:
            box += 54
        else:
            box += 108

        return box

    def agent_message(self,inMessage):

        #Message Description
        # save_policy FILENAME
        # Action: Save current value function in binary format to
        # file called FILENAME
        #
        if inMessage.startswith("save policy"):
            splitString=inMessage.split(" ")
            self.save_value_function(splitString[1])
            print "Saved."
            return "message understood, saving policy"

        #Message Description
        # load_policy FILENAME
        # Action: Load value function in binary format from
        # file called FILENAME
        #
        #if inMessage.startswith("load policy"):
        #    splitString=inMessage.split(" ")
        #    self.load_value_function(splitString[1])
        #    print "Loaded."
        #    return "message understood, loading policy"

        return "SuttonAgent(Python) does not understand your message."

    def save_value_function(self, fileName):
        theFile = open(fileName, "w")
        pickle.dump(self.w, theFile)
        theFile.close()


if __name__=="__main__":
    AgentLoader.loadAgent(SuttonAgent())