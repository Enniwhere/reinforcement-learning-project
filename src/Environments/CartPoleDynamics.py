
import random
import sys
from math import *


g = 9.8
cartMass = 1.0
poleMass = 0.1
totalMass = poleMass + cartMass
poleLength = 0.5
poleMassLength = poleMass*poleLength
tau = 0.02

    
class CartPoleDynamics():

    def __init__(self,x,xvel,theta,thetavel):
        self.x = x
        self.xvel = xvel
        self.theta = theta
        self.thetavel = thetavel


    def UpdateValues(self,F):
        global g, cartMass, poleMass, totalMass, poleLength, poleMassLength, tau
        temp = (F + poleMassLength * pow(self.thetavel,2) * sin(self.theta))/totalMass
        thetaAcc = (g * sin(self.theta) - cos(self.theta) * temp) \
                   /(poleLength *(4.0/3.0 - poleMass * pow(cos(self.theta),2) \
                                  /totalMass))

        xAcc = temp - poleMassLength * thetaAcc * cos(self.theta)/totalMass

        self.x = self.x + self.xvel * tau
        self.xvel = self.xvel + xAcc * tau
        self.theta = self.theta + self.thetavel * tau
        self.thetavel = self.thetavel + thetaAcc * tau

        #print "Theta Acceleration: " + str(thetaAcc)
        #print "Theta Velocity: " + str(self.thetaVel)
        #print "Theta: " + str(self.theta)
        #print "xAcc: " + str(xAcc)
        #print "xVel: " + str(self.xVel)
        #print "x: " + str(self.x)
