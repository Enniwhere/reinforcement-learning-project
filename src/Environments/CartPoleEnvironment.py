import random
import sys
from math import *
from math import pi as PI
from CartPoleDynamics import *
from rlglue.environment.Environment import Environment
from rlglue.environment import EnvironmentLoader as EnvironmentLoader
from rlglue.types import Observation
from rlglue.types import Action
from rlglue.types import Reward_observation_terminal


g = 9.82
cartMass = 1.0
poleMass = 0.1
totalMass = poleMass + cartMass
poleLength = 0.5
poleMassLength = poleMass*poleLength
tau = 0.02


class CartPoleEnvironment(Environment):

    randGenerator=random.Random()
    dynamics = None
    lastTheta = 0
    episodeOver = 0


    def env_init(self):

        return "VERSION RL-Glue-3.0 PROBLEMTYPE episodic DISCOUNTFACTOR 1.0 OBSERVATIONS DOUBLES (-2.4 2.4)(-1.3 1.3)(-0.1 0.1)(-1.8 1.8) ACTIONS DOUBLES (-10.0 10.0)  REWARDS (0 1)  EXTRA CartPoleEnvironment by Simon Enni."

    def env_start(self):
        self.num_steps = 0
        self.episodeOver = 0
        randTheta = self.randGenerator.random()/50.0 - 0.01
        self.dynamics = CartPoleDynamics(0.0,0.0,randTheta,0.0)
        print "Started new episode with start theta " + str(randTheta)
        #self.dynamics = CartPoleDynamics(0.0,0.0,0.0,0.0)

        returnObs=Observation()
        returnObs.doubleArray=[self.dynamics.x,self.dynamics.xvel,self.dynamics.theta,self.dynamics.thetavel]
        self.lastTheta = self.dynamics.theta
        return returnObs

    def env_step(self,thisAction):
        if self.episodeOver == 1:
            theObs=Observation()
            theObs.doubleArray=[self.dynamics.x,self.dynamics.xvel,self.dynamics.theta,self.dynamics.thetavel]

            returnRO=Reward_observation_terminal()
            returnRO.r=0.0
            theObs=Observation()
            theObs.doubleArray=[self.dynamics.x,self.dynamics.xvel,self.dynamics.theta,self.dynamics.thetavel]
            returnRO.o=theObs
            returnRO.terminal=1

            return returnRO

        episodeOver=0
        theReward=0.0
        F = thisAction.doubleArray[0]
        print "Agent took action " + str(F) + " for theta " + str(self.dynamics.theta) + " and thetavel " + str(self.dynamics.thetavel)

        self.lastTheta = self.dynamics.theta
        self.dynamics.UpdateValues(F)

        if abs(self.dynamics.theta) > 12*PI/360:
            print "Theta got violated: " + str(self.dynamics.theta)
            theReward=-1.0
            #self.episodeOver=1
            episodeOver = 1
        elif abs(self.dynamics.x) > 2.4:
            print "x got violated: " + str(self.dynamics.x)
            theReward=-1.0
            #self.episodeOver=1
            episodeOver = 1
        else:
            #theReward = 12*PI/360 - abs(self.dynamics.theta)
            #theReward = 0.0
            theReward = (abs(self.lastTheta) - abs(self.dynamics.theta))*1.0



        theObs=Observation()
        theObs.doubleArray=[self.dynamics.x,self.dynamics.xvel,self.dynamics.theta,self.dynamics.thetavel]

        returnRO=Reward_observation_terminal()
        returnRO.r=theReward
        returnRO.o=theObs
        returnRO.terminal=episodeOver

        return returnRO


    def env_cleanup(self):
        pass

    def env_message(self,inMessage):
        if inMessage=="what is your name?":
            return "my name is CartPole, Python edition!";
        else:
            return "I don't know how to respond to your message";



if __name__=="__main__":
    EnvironmentLoader.loadEnvironment(CartPoleEnvironment())