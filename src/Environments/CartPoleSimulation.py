__author__ = 'simon'

from CartPoleGraphics import *
import random
import numpy as np
from rlglue.utils import TaskSpecVRLGLUE3
from math import pi as PI
import math

DELAY = 100


class CartPoleWindow(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)

        parent.title('CartPole')
        self.canvas = CartPoleSimulation(parent)
        self.pack()

class CartPoleSimulation(Canvas):

    value_function = None
    dyn = None
    randGenerator=random.Random()
    observationRanges = None
    window = None
    numStates = (20,20,20,20,10)

    def __init__(self, parent):

        Canvas.__init__(self, width=WIDTH, height=HEIGHT,
            background="white", highlightthickness=0)

        self.parent = parent
        self.pack()
        self.cartX = 300
        self.cartY = 300
        self.cartW = 120
        self.cartH = 40
        self.poleLength = 200
        self.cart = self.create_rectangle(self.cartX, self.cartY, self.cartX+self.cartW, self.cartY+self.cartH,outline="#05f", fill="#05f")
        self.pole = self.create_line(self.cartX+self.cartW/2,self.cartY,self.cartX+self.cartW/2,self.cartY-100)
        #self.bind_all("<Key>", self.onKeyPressed)


        self.load_value_function("policy")
        self.dyn = CartPoleDynamics(0.0,0.0,self.randGenerator.random()/2 - 0.25,0.0)
        #self.dyn = CartPoleDynamics(0.0,0.0,0.25,0.0)
        TaskSpec = TaskSpecVRLGLUE3.TaskSpecParser("VERSION RL-Glue-3.0 PROBLEMTYPE episodic DISCOUNTFACTOR 1.0 OBSERVATIONS DOUBLES (-5 5)(-2 2)(-1.2 1.2)(-0.5 0.5) ACTIONS DOUBLES (-10.0 10.0)  REWARDS (-10.0 10.0)  EXTRA CartPoleEnvironment by Simon Enni.")
        self.observationRanges = TaskSpec.getDoubleObservations()
        self.updateCart(self.dyn.x)
        self.updatePole(self.dyn.x,self.dyn.theta)
        self.after(DELAY, self.simLoop)

    def updateCart(self,x):
        self.coords(self.cart,x*200+xorigin,self.cartY,x*200+xorigin+self.cartW,self.cartY+self.cartH)

    def updatePole(self,x,theta):
        polex1 = x*200+xorigin+self.cartW/2
        poley1 = self.cartY
        polex2 = polex1 + sin(theta) * self.poleLength
        poley2 = poley1 - cos(theta) * self.poleLength
        self.coords(self.pole,polex1,poley1,polex2,poley2)

    def load_value_function(self, fileName):
        theFile = open(fileName, "r")
        self.value_function=pickle.load(theFile)
        theFile.close()

    def getValueFunctionRow(self, x, xvel, theta, thetavel):

        xint = round((x-self.observationRanges[0][0])*self.numStates[0]/(self.observationRanges[0][1] - self.observationRanges[0][0]))
        xvelint = round((xvel-self.observationRanges[1][0])*self.numStates[1]/(self.observationRanges[1][1] - self.observationRanges[1][0]))
        thetaint = round((theta-self.observationRanges[2][0])*self.numStates[1]/(self.observationRanges[2][1] - self.observationRanges[2][0]))
        thetavelint = round((thetavel-self.observationRanges[3][0])*self.numStates[1]/(self.observationRanges[3][1] - self.observationRanges[3][0]))

        if xint < 0:
            xint = 0
        if xint > self.numStates[0]-1:
            xint = self.numStates[0]-1
        if xvelint < 0:
            xvelint = 0
        if xvelint > self.numStates[1]-1:
            xvelint = self.numStates[1]-1
        if thetaint > self.numStates[2]-1:
            thetaint = self.numStates[2]-1
        if thetaint < 0:
            thetaint = 0
        if thetavelint < 0:
            thetavelint = 0
        if thetavelint > self.numStates[3]-1:
            thetavelint = self.numStates[3]-1

        return self.value_function[xint][xvelint][thetaint][thetavelint]

    def pickAction(self, state):
        return np.argmax(self.getValueFunctionRow(state[0],state[1],state[2],state[3]))

    def getBox(self,x,xvel,theta,thetavel):
        box = 0
        one_degree = 2.0*PI/360.0

        if x < -0.8:
            box = 0
        elif x < 0.8:
            box = 1
        else:
            box = 2

        if xvel < -0.5:
            pass
        elif xvel < 0.5:
            box += 3
        else:
            box += 6

        if theta < -6*one_degree:
            pass
        elif theta < -one_degree:
            box += 9
        elif theta < 0:
            box += 18
        elif theta < one_degree:
            box += 27
        elif theta < 6*one_degree:
            box += 36
        else:
            box += 45

        if thetavel < -50*one_degree:
            pass
        elif thetavel < 50*one_degree:
            box += 54
        else:
            box += 108

        return box

    def prob_push_right(self,s):
        return 1.0 / (1.0 + math.exp(-max(-50.0, min(s, 50.0))))

    def get_action(self,box):

        choice = random.random() < self.prob_push_right(self.value_function[box])
        #print "Choice of action was " + str(choice) + " which was chosen with probability " + str(self.prob_push_right(self.w[box])) + "\n"
        if not choice:
            return -10.0
        else:
            return 10.0

    def simLoop(self):
        self.dyn.UpdateValues(self.action_double(self.get_action(self.getBox(self.dyn.x,self.dyn.xvel,self.dyn.theta,self.dyn.thetavel))))
        self.dyn.UpdateValues(0.0)
        self.checkForFailure()
        self.updateCart(self.dyn.x)
        self.updatePole(self.dyn.x,self.dyn.theta)
        self.after(DELAY, self.simLoop)

    def checkForFailure(self):
        if abs(self.dyn.theta) > 1.2 or abs(self.dyn.x) > 5:
            self.dyn = CartPoleDynamics(0.0,0.0,self.randGenerator.random()/2 - 0.25,0.0)

    def action_double(self,intAction):
        return -10.0 + intAction*2.0

def main():
    root = Tk()
    sim = CartPoleWindow(root)
    root.mainloop()

if __name__ == '__main__':
    main()