#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
ZetCode Tkinter tutorial

This script shows a simple window
on the screen.

author: Jan Bodnar
last modified: January 2011
website: www.zetcode.com
"""

import sys
import random
from CartPoleDynamics import *
from Tkinter import *
import pickle

WIDTH = 640
HEIGHT = 400
dt = 50
xorigin = 300


class CartPoleWindow(Frame):

    def __init__(self, parent, x, theta):
        Frame.__init__(self, parent)
                
        parent.title('CartPole')
        self.canvas = CartPoleCanvas(parent,x,theta)
        self.pack()
               

        
class CartPoleCanvas(Canvas):



    def __init__(self, parent, x, theta):

        Canvas.__init__(self, width=WIDTH, height=HEIGHT, 
            background="white", highlightthickness=0)
         
        self.parent = parent 
        self.pack()
        self.cartX = 300
        self.cartY = 300
        self.cartW = 120
        self.cartH = 40
        self.poleLength = 200
        self.cart = self.create_rectangle(self.cartX, self.cartY, self.cartX+self.cartW, self.cartY+self.cartH,outline="#05f", fill="#05f")
        self.pole = self.create_line(self.cartX+self.cartW/2,self.cartY,self.cartX+self.cartW/2,self.cartY-100)
        #self.bind_all("<Key>", self.onKeyPressed)
        self.left = True
        self.right = False
        self.inGame = True
        self.updateCart(x)
        self.updatePole(x,theta)

    def updateCart(self,x):
        self.coords(self.cart,x*200+xorigin,self.cartY,x*200+xorigin+self.cartW,self.cartY+self.cartH)

    def updatePole(self,x,theta):
        polex1 = x*200+xorigin+self.cartW/2
        poley1 = self.cartY
        polex2 = polex1 + sin(theta) * self.poleLength
        poley2 = poley1 - cos(theta) * self.poleLength
        self.coords(self.pole,polex1,poley1,polex2,poley2)

    

